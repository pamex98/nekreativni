package hr.fer.iznajmiromobil.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.iznajmiromobil.AbstractControllerTest;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.service.UserService;

@Transactional
public class UserControllerTest extends AbstractControllerTest {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder coder;

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testCreateUser() throws Exception {

		String uri = "/api/users";
		User user = new User();
		user.setPassword(coder.encode("admin"));
		user.setUsername("user4");
		String inputJson = super.mapToJson(user);

		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();

		int status = result.getResponse().getStatus();

		Assert.assertEquals("failure - expected HTTP status", 400, status);

		user.setUsername("user0");
		user.setName("Mirko");
		user.setSurname("Pasic");
		user.setEmailAdress("pm50827@fer.hr");
		user.setCardNumber("0321 3214 2343");
		user.setUrlOsobna("slika osobne");
		user.setUrlPotvrdaNekazna("slika potvrde");
		inputJson = super.mapToJson(user);

		result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
		String content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		// found code (EntityAlreadyExistsException), username
		Assert.assertEquals(content, 302, status);

		user.setUsername("user20");
		inputJson = super.mapToJson(user);

		result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
		content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		Assert.assertEquals(content, 201, status);

	}

	@Test
	public void testUpdateUser() throws Exception {

		String uri = "/api/users/{id}";

		User old = userService.findByUsername("user0");
		
		String inputJson = super.mapToJson(old);

		MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri, 123)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(inputJson))
				.andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		//krivi je ID 
		Assert.assertEquals(content, 400, status);

	}

}
