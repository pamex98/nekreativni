package hr.fer.iznajmiromobil.service;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.iznajmiromobil.AbstractTest;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;
import hr.fer.iznajmiromobil.service.exceptions.EntityAlreadyExistsException;
import hr.fer.iznajmiromobil.service.exceptions.EntityMissingException;

@Transactional
public class UserServiceTest extends AbstractTest {

	@Autowired
	private UserService service;

	@Autowired
	private PasswordEncoder pass;

	@Test
	public void testFindAll() {

		Collection<User> list = service.findAll();

		Assert.assertNotNull("failure - expected not null", list);
		Assert.assertEquals("failure - expected list size", 16, list.size());

	}

	@Test
	public void testCreateUser() {
		User user = new User();
		user.setUsername("user0");
		user.setPassword(pass.encode("admin"));
		Exception e = null;

		try {
			service.createUser(user);
		} catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assert.assertTrue("failure - expected IllegalArgumentException", e instanceof IllegalArgumentException);

		Exception e2 = null;
		user.setId(2L);
		try {
			service.createUser(user);
		} catch (IllegalArgumentException ex) {
			e2 = ex;
		}

		Assert.assertTrue("failure - expected IllegalArgumentException",
				e2.getMessage().equals("User id mora biti null, a ne " + user.getId()));

		Exception e3 = null;
		User userRight = new User();
		userRight.setPassword(pass.encode("admin"));
		// dbinit postoji taj username
		userRight.setUsername("user0");
		userRight.setName("Mirko");
		userRight.setSurname("Pasic");
		userRight.setEmailAdress("pm50827@fer.hr");
		userRight.setCardNumber("0321 3214 2343");
		userRight.setUrlOsobna("slika osobne");
		userRight.setUrlPotvrdaNekazna("slika potvrde");

		try {
			service.createUser(userRight);
		} catch (EntityAlreadyExistsException e4) {
			e3 = e4;
		}

		Assert.assertTrue("failure - expected EntityAlreadyExistsException",
				e3 instanceof EntityAlreadyExistsException);

		Exception e4 = null;
		userRight.setUsername("user5");
		userRight.setActive(true);
		try {
			service.createUser(userRight);
		} catch (IllegalArgumentException e5) {
			e4 = e5;
		}

		Assert.assertTrue("Expected IllegalArgumentException",
				e4.getMessage().equals("Početno registrirani user ne smije biti aktivan!"));

		Exception e5 = null;
		userRight.setUsername("user20");
		userRight.setActive(false);
		try {
			service.createUser(userRight);
		} catch (Exception e6) {
			e5 = e6;
		}

		Assert.assertTrue("There should be no exceptions", e5 == null);
	}

	@Test
	public void testUpdateUser() {
		User user = service.findByUsername("user0");
		Assert.assertTrue(user.isActive());
		user.setActive(false);
		user = service.updateUser(user);
		Assert.assertTrue(!user.isActive());
	}

	@Test
	public void testFindByUsername() {
		service.findByUsername("user0");
		Exception e = null;
		try {
			service.findByUsername("user131");
		} catch (EntityMissingException e2) {
			e = e2;
		}

		Assert.assertTrue("EntityMissingException expected", e instanceof EntityMissingException);
	}

	@Test
	public void testFindByRole() {
		Collection<User> useri = service.findByRole(Role.USER);
		for (User f : useri) {
			Assert.assertTrue(f.getUsername() + " nije USER", f.getRole().equals(Role.USER));
		}
	}

}
