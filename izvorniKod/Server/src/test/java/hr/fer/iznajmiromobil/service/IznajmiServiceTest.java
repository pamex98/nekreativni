package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.iznajmiromobil.AbstractTest;
import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Romobil;
import hr.fer.iznajmiromobil.service.exceptions.EntityAlreadyExistsException;

@Transactional
public class IznajmiServiceTest extends AbstractTest {

	@Autowired
	private IznajmiService service;

	@Autowired
	private UserService userService;

	@Autowired
	private RomobilService romobilService;

	@Test
	public void testCreateIznajmi() {
		Iznajmi iznajmi = new Iznajmi();
		iznajmi.setId(2l);

		Exception e = null;
		try {
			service.createIznajmi(iznajmi);
		} catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assert.assertTrue("Expected IllegalArgumentException", e.getMessage().equals("Id mora biti null"));

		Iznajmi iznajmi2 = new Iznajmi();
		iznajmi2.setKlijent(userService.findByUsername("user0"));
		try {
			service.createIznajmi(iznajmi2);
		} catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assert.assertTrue("Expected IllegalArgumentException", e.getMessage().equals("Romobil ne može biti null"));

		List<Romobil> romobili = romobilService.findAll();

		iznajmi2.setRomobil(romobili.get(1));

		Iznajmi resp = service.createIznajmi(iznajmi2);

		Assert.assertTrue("Greska u spremanju iznajmia", resp != null);

		Iznajmi iznajmi3 = new Iznajmi();
		iznajmi3.setKlijent(userService.findByUsername("user0"));
		iznajmi3.setRomobil(romobili.get(2));
		e = null;
		try {
			service.createIznajmi(iznajmi3);
		} catch (EntityAlreadyExistsException e2) {
			e = e2;
		}
		Assert.assertTrue("EntityAlreadyExistsException expected",
				e.getMessage().equals("Vec imate zahtjev za iznajmljivanjem"));

		iznajmi3.setKlijent(userService.findByUsername("user2"));
		iznajmi3.setRomobil(romobili.get((1)));
		e = null;
		try {
			service.createIznajmi(iznajmi3);
		} catch (Exception e2) {
			e = e2;
		}
		Assert.assertTrue("EntityAlreadyExistsException expected",
				e.getMessage().equals("Za navedeni romobil postoji zahtjev"));

		iznajmi3.setRomobil(romobili.get((2)));
		iznajmi3.setKlijent(userService.findByUsername("user2"));
		e = null;
		try {
			service.createIznajmi(iznajmi3);
		} catch (IllegalArgumentException e2) {
			e = e2;
		}
		Assert.assertTrue("IllegalArgumentException expected",
				e.getMessage().equals("Ne mozete iznajmiti svoj romobil"));

	}

	@Test
	public void testUpdateIznajmi() {
		Iznajmi iznajmi = new Iznajmi();
		iznajmi.setKlijent(userService.findByUsername("user0"));
		List<Romobil> romobili = romobilService.findAll();
		iznajmi.setRomobil(romobili.get(1));
		iznajmi = service.createIznajmi(iznajmi);

		Iznajmi update = new Iznajmi();
		update.setId(iznajmi.getId());
		Exception e = null;
		try {
			service.updateIznajmi(update);
		} catch (IllegalArgumentException e2) {
			e = e2;
		}
		Assert.assertTrue("IllegalArgumentException expected", e.getMessage().equals("Klijent ne može biti null"));

		update.setKlijent(userService.findByUsername("user1"));
		update.setRomobil(romobili.get(1));

		e = null;
		try {
			service.updateIznajmi(update);
		} catch (IllegalArgumentException e2) {
			e = e2;
		}
		Assert.assertTrue("IllegalArgumentException expected", e.getMessage().equals("Klijent se ne smije mijenjati"));

		update.setKlijent(userService.findByUsername("user0"));
		update.setRomobil(romobili.get(3));
		e = null;
		try {
			service.updateIznajmi(update);
		} catch (IllegalArgumentException e2) {
			e = e2;
		}
		Assert.assertTrue("IllegalArgumentException expected", e.getMessage().equals("Romobil id mora biti isti!"));
		
		

	}

}
