package hr.fer.iznajmiromobil.controller;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import hr.fer.iznajmiromobil.AbstractControllerTest;
import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Romobil;

import hr.fer.iznajmiromobil.service.IznajmiService;
import hr.fer.iznajmiromobil.service.RomobilService;
import hr.fer.iznajmiromobil.service.UserService;

@Transactional
public class IznajmiControllerTest extends AbstractControllerTest {

	@Autowired
	private IznajmiService iznajmiService;

	@Autowired
	private UserService userService;

	@Autowired
	private RomobilService romobilService;

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testCreateIznajmi() throws Exception {

		String uri = "/api/iznajmi";
		Iznajmi iznajmi = new Iznajmi();

		String inputJson = super.mapToJson(iznajmi);

		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		Assert.assertEquals(content, 400, status);

		List<Romobil> romobili = romobilService.findAll();
		iznajmi.setRomobil(romobili.get(0));
		iznajmi.setKlijent(userService.findByUsername("user0"));
		inputJson = super.mapToJson(iznajmi);

		result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
		content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		// ne može svoj romobil
		Assert.assertEquals(content, 400, status);

		iznajmi.setRomobil(romobili.get(1));
		inputJson = super.mapToJson(iznajmi);

		result = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
		content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		Assert.assertEquals(content, 201, status);

	}

	@Test
	public void testUpdateIznajmi() throws Exception {

		String uri = "/api/iznajmi/{id}";
		Iznajmi iznajmi = new Iznajmi();
		List<Romobil> romobili = romobilService.findAll();
		iznajmi.setRomobil(romobili.get(1));
		iznajmi.setKlijent(userService.findByUsername("user0"));
		iznajmi = iznajmiService.createIznajmi(iznajmi);

		Iznajmi novi = new Iznajmi();
		novi.setId(iznajmi.getId());
		novi.setKlijent(userService.findByUsername("user0"));
		novi.setRomobil(romobili.get(0));

		String inputJson = super.mapToJson(novi);

		MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri, novi.getId())
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(inputJson))
				.andReturn();

		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();

		// ne mozes mijenjati romobil
		Assert.assertEquals(content, 400, status);

		novi.setRomobil(romobili.get(1));
		novi.setKlijentAccept(true);

		inputJson = super.mapToJson(novi);

		result = mvc.perform(MockMvcRequestBuilders.put(uri, novi.getId()).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();

		content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		// success
		Assert.assertEquals(content, 200, status);

		novi.setKlijentAccept(false);

		inputJson = super.mapToJson(novi);

		result = mvc.perform(MockMvcRequestBuilders.put(uri, 12312).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();

		content = result.getResponse().getContentAsString();
		status = result.getResponse().getStatus();

		// krivi id u url-u
		Assert.assertEquals(content, 400, status);

	}

}
