package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.ZamjenaSlike;

@Service
public interface ZamjenaSlikeService {
	
	ZamjenaSlike zamjenaSlike(ZamjenaSlike zamjena);
	
	ZamjenaSlike updateZamjenu(ZamjenaSlike zamjena);
	
	List<ZamjenaSlike> findByPrijavljena();
	
	List<ZamjenaSlike> findByRomobilOwner(User user);
	
}
