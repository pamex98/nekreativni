package hr.fer.iznajmiromobil.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.ZamjenaSlike;
import hr.fer.iznajmiromobil.repository.ZamjenaSlikeRepository;
import hr.fer.iznajmiromobil.service.ZamjenaSlikeService;

@Service
public class ZamjenaSlikeServiceJpa implements ZamjenaSlikeService {

	@Autowired
	private ZamjenaSlikeRepository repo;

	@Override
	public ZamjenaSlike zamjenaSlike(ZamjenaSlike zamjena) {
		return repo.save(zamjena);
	}
	
	
	//provjeri jel radi ovo
	@Override
	public List<ZamjenaSlike> findByPrijavljena() {
		return repo.findAllByPrijavljena();
	}


	@Override
	public ZamjenaSlike updateZamjenu(ZamjenaSlike zamjena) {
		return repo.save(zamjena);
	}


	@Override
	public List<ZamjenaSlike> findByRomobilOwner(User user) {
		return repo.findByRomobil_Owner(user);
	}
	
	

}
