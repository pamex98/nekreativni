package hr.fer.iznajmiromobil.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;
import hr.fer.iznajmiromobil.repository.UserRepository;
import hr.fer.iznajmiromobil.service.UserService;
import hr.fer.iznajmiromobil.service.exceptions.EntityAlreadyExistsException;
import hr.fer.iznajmiromobil.service.exceptions.EntityMissingException;

@Service
public class UserServiceJpa implements UserService {

	@Autowired
	private UserRepository repo;

	@Autowired
	private PasswordEncoder passEncoder;

	@Override
	public User createUser(User user) {
		Assert.isNull(user.getId(), "User id mora biti null, a ne " + user.getId());
		Assert.notNull(user.getUsername(), "Username ne smije biti null");
		Assert.notNull(user.getPassword(), "Password ne smije biti null");
		Assert.notNull(user.getCardNumber(), "Card number ne smije biti null");
		Assert.notNull(user.getEmailAdress(), "Email ne smije biti null");
		Assert.notNull(user.getSurname(), "Prezime ne smije biti null");
		Assert.notNull(user.getUrlOsobna(), "Slika osobne ne smije biti null");
		Assert.notNull(user.getUrlPotvrdaNekazna(), "Slika potvrde ne smije biti null");
		Assert.notNull(user.getName(), "Ime ne smije biti null");
		Assert.isTrue(!user.isActive(), "Početno registrirani user ne smije biti aktivan!");
		if (repo.existsByUsername(user.getUsername())) {
			throw new EntityAlreadyExistsException("Username već postoji");
		}
		user.setPassword(passEncoder.encode(user.getPassword()));
		return repo.save(user);
	}

	@Override
	public boolean deactivateAll() {
		List<User> svi = findByRole(Role.USER);
		for (User f : svi) {
			f.setActive(false);
			updateUser(f);
		}
		return true;
	}

	@Override
	public User updateUser(User user) {
		Assert.notNull(user.getUsername(), "Username ne smije biti null");
		Assert.notNull(user.getPassword(), "Password ne smije biti null");
		Assert.notNull(user.getCardNumber(), "Card number ne smije biti null");
		Assert.notNull(user.getEmailAdress(), "Email ne smije biti null");
		Assert.notNull(user.getSurname(), "Prezime ne smije biti null");
		Assert.notNull(user.getUrlOsobna(), "Slika osobne ne smije biti null");
		Assert.notNull(user.getUrlPotvrdaNekazna(), "Slika potvrde ne smije biti null");
		Assert.notNull(user.getName(), "Ime ne smije biti null");
		return repo.save(user);
	}

	@Override
	public List<User> findAll() {
		return repo.findAll();
	}

	@Override
	public User findByUsername(String username) {
		if (!repo.existsByUsername(username)) {
			throw new EntityMissingException("Korisnik " + username + " ne postoji");
		}
		return repo.findByUsername(username);
	}

	@Override
	public List<User> findByRole(Role role) {
		return repo.findByRole(role);
	}

	@Override
	public List<User> findByActive(boolean active) {
		return repo.findByIsActive(active);
	}

}
