package hr.fer.iznajmiromobil.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class Poruka {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	private User posiljatelj;

	@ManyToOne(cascade = CascadeType.MERGE)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Iznajmi uzZahtjev;

	@Column(nullable = false)
	private String poruka;

	private Date datumPoruke = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getPosiljatelj() {
		return posiljatelj;
	}

	public void setPosiljatelj(User posiljatelj) {
		this.posiljatelj = posiljatelj;
	}

	public Iznajmi getUzZahtjev() {
		return uzZahtjev;
	}

	public void setUzZahtjev(Iznajmi uzZahtjev) {
		this.uzZahtjev = uzZahtjev;
	}

	public String getPoruka() {
		return poruka;
	}

	public void setPoruka(String poruka) {
		this.poruka = poruka;
	}

	public Date getDatumPoruke() {
		return datumPoruke;
	}

	public void setDatumPoruke(Date datumPoruke) {
		this.datumPoruke = datumPoruke;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datumPoruke == null) ? 0 : datumPoruke.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((poruka == null) ? 0 : poruka.hashCode());
		result = prime * result + ((posiljatelj == null) ? 0 : posiljatelj.hashCode());
		result = prime * result + ((uzZahtjev == null) ? 0 : uzZahtjev.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poruka other = (Poruka) obj;
		if (datumPoruke == null) {
			if (other.datumPoruke != null)
				return false;
		} else if (!datumPoruke.equals(other.datumPoruke))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (poruka == null) {
			if (other.poruka != null)
				return false;
		} else if (!poruka.equals(other.poruka))
			return false;
		if (posiljatelj == null) {
			if (other.posiljatelj != null)
				return false;
		} else if (!posiljatelj.equals(other.posiljatelj))
			return false;
		if (uzZahtjev == null) {
			if (other.uzZahtjev != null)
				return false;
		} else if (!uzZahtjev.equals(other.uzZahtjev))
			return false;
		return true;
	}

}
