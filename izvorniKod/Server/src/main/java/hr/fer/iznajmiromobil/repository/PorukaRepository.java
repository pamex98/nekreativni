package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Poruka;

@Repository
public interface PorukaRepository extends JpaRepository<Poruka, Long> {
	
	List<Poruka> findByUzZahtjev(Iznajmi zahtjev);
	
}
