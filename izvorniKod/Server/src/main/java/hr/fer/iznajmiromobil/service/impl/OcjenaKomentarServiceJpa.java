package hr.fer.iznajmiromobil.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.OcjenaKomentar;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.repository.OcjenaKomentarRepository;
import hr.fer.iznajmiromobil.service.OcjenaKomentarService;

@Service
public class OcjenaKomentarServiceJpa implements OcjenaKomentarService {

	@Autowired
	private OcjenaKomentarRepository repo;

	@Override
	public OcjenaKomentar createOcjenaKomentar(OcjenaKomentar ocjKomm) {
		return repo.save(ocjKomm);
	}

	@Override
	public List<OcjenaKomentar> findByOwner(User user) {
		return repo.findByKlijent(user);
	}

	@Override
	public List<OcjenaKomentar> findByOcjenivac(User user) {
		return repo.findByOcjenivac(user);
	}

	@Override
	public OcjenaKomentar updateOcjenaKomentar(OcjenaKomentar ocjKomm) {
		return repo.save(ocjKomm);
	}

}
