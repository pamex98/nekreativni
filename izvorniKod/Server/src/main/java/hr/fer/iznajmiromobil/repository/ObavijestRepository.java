package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.Obavijest;
import hr.fer.iznajmiromobil.domain.User;

@Repository
public interface ObavijestRepository extends JpaRepository<Obavijest, Long> {
	
	List<Obavijest> findByOwner(User owner);
	
}
