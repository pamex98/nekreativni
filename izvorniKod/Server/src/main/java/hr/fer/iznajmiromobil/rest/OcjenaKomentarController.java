package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.OcjenaKomentar;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.service.OcjenaKomentarService;

@RestController
@RequestMapping("/api/ocjenaKomentar")
public class OcjenaKomentarController {
	
	
	@Autowired
	private OcjenaKomentarService service;
	
	
	@PostMapping("")
	public ResponseEntity<OcjenaKomentar> createOcjenaKomentar(@RequestBody OcjenaKomentar ocjenaKomm){
		OcjenaKomentar saved = service.createOcjenaKomentar(ocjenaKomm);
		return ResponseEntity.created(URI.create("/ocjenaKomentar/"+saved.getId())).body(saved);
	}
	
	//get nema body..
	@PostMapping("/owner")
	public List<OcjenaKomentar> getByOwner(@RequestBody User owner){
		return service.findByOwner(owner);
	}
	
	@PostMapping("/ocjenivac")
	public List<OcjenaKomentar> getByOcjenivac(@RequestBody User user){
		return service.findByOcjenivac(user);
	}
	
	@PutMapping("")
	public OcjenaKomentar updateOcjenaKomm(@RequestBody OcjenaKomentar ocjenaKomm) {
		return service.updateOcjenaKomentar(ocjenaKomm);
	}
	
	
}
