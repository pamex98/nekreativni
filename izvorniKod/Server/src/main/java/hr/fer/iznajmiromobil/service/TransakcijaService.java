package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Transakcija;
import hr.fer.iznajmiromobil.domain.User;

@Service
public interface TransakcijaService {
	
	
	Transakcija createTransakcija(Transakcija transakcija);
	
	List<Transakcija> findByUser(User user);

}
