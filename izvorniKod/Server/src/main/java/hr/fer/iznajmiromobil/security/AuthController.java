package hr.fer.iznajmiromobil.security;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.service.UserService;

@RestController
@RequestMapping("/api/security")
public class AuthController {
	
	@Autowired
	private UserService service;
	
	@GetMapping(path="/auth")
	public User auth(Principal user) {
		return service.findByUsername(user.getName());
	}
	
}
