package hr.fer.iznajmiromobil.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import hr.fer.iznajmiromobil.domain.enums.Role;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Enumerated(EnumType.STRING)
	private Role role = Role.USER;

	private boolean isActive = false;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String surname;

	@Column(nullable = false)
	private String cardNumber;

	@Column(nullable = false)
	private String emailAdress;

	@Column(columnDefinition = "LONGTEXT", nullable = false)
	private String urlOsobna;

	@Column(columnDefinition = "LONGTEXT", nullable = false)
	private String urlPotvrdaNekazna;

	private boolean isNamePrivate;

	private boolean isSurnamePrivate;

	private boolean isCardNumberPrivate;

	private boolean isEmailAdressPrivate;

	private boolean isOsobnaPrivate;

	private boolean isPotvrdaNekaznaPrivate;

	public User() {
	}

	public User(String username, String password, Role role, boolean isActive) {
		this.username = username;
		this.password = password;
		this.role = role;
		this.isActive = isActive;
	}

	public String getUrlOsobna() {
		return urlOsobna;
	}

	public void setUrlOsobna(String urlOsobna) {
		this.urlOsobna = urlOsobna;
	}

	public String getUrlPotvrdaNekazna() {
		return urlPotvrdaNekazna;
	}

	public void setUrlPotvrdaNekazna(String urlPotvrdaNekazna) {
		this.urlPotvrdaNekazna = urlPotvrdaNekazna;
	}

	public boolean isOsobnaPrivate() {
		return isOsobnaPrivate;
	}

	public void setOsobnaPrivate(boolean isOsobnaPrivate) {
		this.isOsobnaPrivate = isOsobnaPrivate;
	}

	public boolean isPotvrdaNekaznaPrivate() {
		return isPotvrdaNekaznaPrivate;
	}

	public void setPotvrdaNekaznaPrivate(boolean isPotvrdaNekaznaPrivate) {
		this.isPotvrdaNekaznaPrivate = isPotvrdaNekaznaPrivate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getEmailAdress() {
		return emailAdress;
	}

	public void setEmailAdress(String emailAdress) {
		this.emailAdress = emailAdress;
	}

	public boolean isNamePrivate() {
		return isNamePrivate;
	}

	public void setNamePrivate(boolean isNamePrivate) {
		this.isNamePrivate = isNamePrivate;
	}

	public boolean isSurnamePrivate() {
		return isSurnamePrivate;
	}

	public void setSurnamePrivate(boolean isSurnamePrivate) {
		this.isSurnamePrivate = isSurnamePrivate;
	}

	public boolean isCardNumberPrivate() {
		return isCardNumberPrivate;
	}

	public void setCardNumberPrivate(boolean isCardNumberPrivate) {
		this.isCardNumberPrivate = isCardNumberPrivate;
	}

	public boolean isEmailAdressPrivate() {
		return isEmailAdressPrivate;
	}

	public void setEmailAdressPrivate(boolean isEmailAdressPrivate) {
		this.isEmailAdressPrivate = isEmailAdressPrivate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
		result = prime * result + ((emailAdress == null) ? 0 : emailAdress.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (isActive ? 1231 : 1237);
		result = prime * result + (isCardNumberPrivate ? 1231 : 1237);
		result = prime * result + (isEmailAdressPrivate ? 1231 : 1237);
		result = prime * result + (isNamePrivate ? 1231 : 1237);
		result = prime * result + (isOsobnaPrivate ? 1231 : 1237);
		result = prime * result + (isPotvrdaNekaznaPrivate ? 1231 : 1237);
		result = prime * result + (isSurnamePrivate ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((urlOsobna == null) ? 0 : urlOsobna.hashCode());
		result = prime * result + ((urlPotvrdaNekazna == null) ? 0 : urlPotvrdaNekazna.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (cardNumber == null) {
			if (other.cardNumber != null)
				return false;
		} else if (!cardNumber.equals(other.cardNumber))
			return false;
		if (emailAdress == null) {
			if (other.emailAdress != null)
				return false;
		} else if (!emailAdress.equals(other.emailAdress))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isActive != other.isActive)
			return false;
		if (isCardNumberPrivate != other.isCardNumberPrivate)
			return false;
		if (isEmailAdressPrivate != other.isEmailAdressPrivate)
			return false;
		if (isNamePrivate != other.isNamePrivate)
			return false;
		if (isOsobnaPrivate != other.isOsobnaPrivate)
			return false;
		if (isPotvrdaNekaznaPrivate != other.isPotvrdaNekaznaPrivate)
			return false;
		if (isSurnamePrivate != other.isSurnamePrivate)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role != other.role)
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		if (urlOsobna == null) {
			if (other.urlOsobna != null)
				return false;
		} else if (!urlOsobna.equals(other.urlOsobna))
			return false;
		if (urlPotvrdaNekazna == null) {
			if (other.urlPotvrdaNekazna != null)
				return false;
		} else if (!urlPotvrdaNekazna.equals(other.urlPotvrdaNekazna))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

}
