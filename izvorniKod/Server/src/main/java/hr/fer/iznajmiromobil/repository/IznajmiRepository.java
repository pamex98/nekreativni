package hr.fer.iznajmiromobil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Romobil;
import hr.fer.iznajmiromobil.domain.User;

@Repository
public interface IznajmiRepository extends JpaRepository<Iznajmi, Long> {
	
	
	Iznajmi findByKlijent(User user);
	
	Iznajmi findByRomobil_Owner(User user);
	
	boolean existsByKlijent(User user);
	
	boolean existsByRomobil_Owner(User user);
	
	boolean existsByRomobil(Romobil romobil);
	
	
	
}
