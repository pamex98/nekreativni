package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;

@Service
public interface UserService {
	
	User createUser(User user);
	
	boolean deactivateAll();
	
	User updateUser(User user);
	
	List<User> findAll();
	
	User findByUsername(String username);
	
	List<User> findByRole(Role role);
	
	List<User> findByActive(boolean active);
	
}
