package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.Transakcija;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.service.TransakcijaService;

@RestController
@RequestMapping("/api/transakcije")
public class TransakcijaController {
	
	@Autowired
	private TransakcijaService service;
	
	@PostMapping("")
	public ResponseEntity<Transakcija> createTransakcija(@RequestBody Transakcija transakcija){
		Transakcija saved = service.createTransakcija(transakcija);
		return ResponseEntity.created(URI.create("/transakcije/"+saved.getId())).body(saved);
	} 
	
	
	//get nema body...
	@PostMapping("/user")
	public List<Transakcija> getByUserTransakcija(@RequestBody User user){
		return service.findByUser(user);
	}
	
	
	
	
	
	
}
