package hr.fer.iznajmiromobil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.repository.IznajmiRepository;
import hr.fer.iznajmiromobil.service.IznajmiService;
import hr.fer.iznajmiromobil.service.exceptions.EntityAlreadyExistsException;
import hr.fer.iznajmiromobil.service.exceptions.EntityMissingException;

@Service
public class IznajmiServiceJpa implements IznajmiService {

	// error handling, optional ? , dodati exception handler

	@Autowired
	private IznajmiRepository repo;

	@Override
	public Iznajmi createIznajmi(Iznajmi zahtjev) {
		Assert.isNull(zahtjev.getId(), "Id mora biti null");
		Assert.notNull(zahtjev.getKlijent(), "Klijent ne može biti null");
		Assert.notNull(zahtjev.getRomobil(), "Romobil ne može biti null");
		if (repo.existsByKlijent(zahtjev.getKlijent())) {
			throw new EntityAlreadyExistsException("Vec imate zahtjev za iznajmljivanjem");
		}
		if (repo.existsByRomobil(zahtjev.getRomobil())) {
			throw new EntityAlreadyExistsException("Za navedeni romobil postoji zahtjev");
		}
		Assert.isTrue(!zahtjev.getKlijent().equals(zahtjev.getRomobil().getOwner()),
				"Ne mozete iznajmiti svoj romobil");
		return repo.save(zahtjev);
	}

	@Override
	public Iznajmi findByUser(User user) {
		if (!repo.existsByKlijent(user)) {
			throw new EntityMissingException("Nemate jos poslani zahtjev za iznajmljivanjem");
		}
		return repo.findByKlijent(user);
	}

	@Override
	public Iznajmi findByRomobilOwner(User owner) {
		if (!repo.existsByRomobil_Owner(owner)) {
			throw new EntityMissingException("Nemate jos poslani zahtjev za iznajmljivanjem");
		}
		return repo.findByRomobil_Owner(owner);
	}

	@Override
	public Iznajmi updateIznajmi(Iznajmi zahtjev) {
		Assert.notNull(zahtjev.getKlijent(), "Klijent ne može biti null");
		Assert.notNull(zahtjev.getRomobil(), "Romobil ne može biti null");
		Iznajmi old = repo.findById(zahtjev.getId()).get();
		Assert.isTrue(zahtjev.getKlijent().getId().equals(old.getKlijent().getId()), "Klijent se ne smije mijenjati");
		Assert.isTrue(zahtjev.getRomobil().getId().equals(old.getRomobil().getId()), "Romobil id mora biti isti!");
		return repo.save(zahtjev);
	}

	@Override
	public void deleteIznajmi(Long id) {
		repo.deleteById(id);
	}

}
