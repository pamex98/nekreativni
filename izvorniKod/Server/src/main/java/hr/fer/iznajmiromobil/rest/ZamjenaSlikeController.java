package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.ZamjenaSlike;
import hr.fer.iznajmiromobil.service.ZamjenaSlikeService;

@RestController
@RequestMapping("/api/zamjenaSlike")
public class ZamjenaSlikeController {
	
	@Autowired
	private ZamjenaSlikeService service;
	
	@PostMapping("")
	public ResponseEntity<ZamjenaSlike> createZamjenaSlike(@RequestBody ZamjenaSlike zamjena){
		ZamjenaSlike saved = service.zamjenaSlike(zamjena);
		return ResponseEntity.created(URI.create("/zamjenaSlike/"+saved.getId())).body(saved);
	}
	
	
	@PutMapping("/{id}")
	public ZamjenaSlike updateZamjenaSlike(@PathVariable Long id, @RequestBody ZamjenaSlike zamjena) {
		if(!zamjena.getId().equals(id)) {
			throw new IllegalArgumentException("Url nije jednak poslanoj zamjeniSlike");
		}
		return service.updateZamjenu(zamjena);
	}
	
	//get nema body...
	@PostMapping("/user")
	public List<ZamjenaSlike> getZamjenaSlike(@RequestBody User user) {
		return service.findByRomobilOwner(user);
	}
	
	@GetMapping("/prijavljene")
	public List<ZamjenaSlike> getByPrijavljene(){
		return service.findByPrijavljena();
	}
	
	
	
	
	
}
