package hr.fer.iznajmiromobil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.Romobil;
import hr.fer.iznajmiromobil.domain.User;

@Repository
public interface RomobilRepository extends JpaRepository<Romobil, Long> {
	
	boolean existsByOwner(User user);
}
