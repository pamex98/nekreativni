package hr.fer.iznajmiromobil.domain;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ZamjenaSlike {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne(cascade=CascadeType.MERGE)
	private Romobil romobil;
	
	private String komentar;
	
	
	@OneToOne
	private User inicijator;
	
	private boolean prijavljena;
	
	@Column(columnDefinition="LONGTEXT")
	private String urlZamjenjenaSlika;
	
	private boolean adminDone;
		
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (adminDone ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inicijator == null) ? 0 : inicijator.hashCode());
		result = prime * result + ((komentar == null) ? 0 : komentar.hashCode());
		result = prime * result + (prijavljena ? 1231 : 1237);
		result = prime * result + ((romobil == null) ? 0 : romobil.hashCode());
		result = prime * result + ((urlZamjenjenaSlika == null) ? 0 : urlZamjenjenaSlika.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZamjenaSlike other = (ZamjenaSlike) obj;
		if (adminDone != other.adminDone)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inicijator == null) {
			if (other.inicijator != null)
				return false;
		} else if (!inicijator.equals(other.inicijator))
			return false;
		if (komentar == null) {
			if (other.komentar != null)
				return false;
		} else if (!komentar.equals(other.komentar))
			return false;
		if (prijavljena != other.prijavljena)
			return false;
		if (romobil == null) {
			if (other.romobil != null)
				return false;
		} else if (!romobil.equals(other.romobil))
			return false;
		if (urlZamjenjenaSlika == null) {
			if (other.urlZamjenjenaSlika != null)
				return false;
		} else if (!urlZamjenjenaSlika.equals(other.urlZamjenjenaSlika))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	

	public boolean isAdminDone() {
		return adminDone;
	}

	public void setAdminDone(boolean adminDone) {
		this.adminDone = adminDone;
	}

	public String getUrlZamjenjenaSlika() {
		return urlZamjenjenaSlika;
	}

	public void setUrlZamjenjenaSlika(String urlZamjenjenaSlika) {
		this.urlZamjenjenaSlika = urlZamjenjenaSlika;
	}

	public Romobil getRomobil() {
		return romobil;
	}

	public void setRomobil(Romobil romobil) {
		this.romobil = romobil;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public User getInicijator() {
		return inicijator;
	}

	public void setInicijator(User inicijator) {
		this.inicijator = inicijator;
	}

	public boolean isPrijavljena() {
		return prijavljena;
	}

	public void setPrijavljena(boolean prijavljena) {
		this.prijavljena = prijavljena;
	}
	
}
