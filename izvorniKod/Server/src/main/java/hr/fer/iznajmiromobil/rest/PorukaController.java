package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Poruka;
import hr.fer.iznajmiromobil.service.PorukaService;

@RestController
@RequestMapping("/api/poruke")
public class PorukaController {
	
	@Autowired
	private PorukaService service;
	
	@PostMapping("")
	public ResponseEntity<Poruka> createPoruka(@RequestBody Poruka poruka){
		Poruka saved = service.createPoruka(poruka);
		return ResponseEntity.created(URI.create("/poruke/"+saved.getId())).body(saved);
	}
	
	
	//get nema body...
	@PostMapping("/iznajmi")
	public List<Poruka> getByIznajmi(@RequestBody Iznajmi iznajmi){
		return service.findByIznajmi(iznajmi);
	}	
	
}
