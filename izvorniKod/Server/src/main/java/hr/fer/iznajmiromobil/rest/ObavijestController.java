package hr.fer.iznajmiromobil.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.Obavijest;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.rest.DTO.StringDTO;
import hr.fer.iznajmiromobil.service.ObavijestService;

@RestController
@RequestMapping("/api/obavijesti")
public class ObavijestController {

	@Autowired
	private ObavijestService service;

	@PostMapping("")
	public Obavijest createObavijest(@RequestBody Obavijest obavijest) {
		return service.createObavijest(obavijest);
	}
	
	
	//get nema body
	@PostMapping("/owner")
	public List<Obavijest> getByOwner(@RequestBody User owner) {
		return service.findByOwner(owner);
	}

	@PutMapping("")
	public Obavijest updateObavijest(@RequestBody Obavijest obavijest) {
		return service.updateObavijest(obavijest);
	}

	@PostMapping("/admin")
	public List<Obavijest> createObavijestAdmin(@RequestBody StringDTO msg) {
		return service.createObavijestAdmin(msg.getValue());
	}

}
