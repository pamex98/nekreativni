package hr.fer.iznajmiromobil.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Transakcija;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.repository.TransakcijaRepository;
import hr.fer.iznajmiromobil.service.TransakcijaService;

@Service
public class TransakcijaServiceJpa implements TransakcijaService {
	
	@Autowired
	private TransakcijaRepository repo;

	@Override
	public Transakcija createTransakcija(Transakcija transakcija) {
		return repo.save(transakcija);
	}

	@Override
	public List<Transakcija> findByUser(User user) {
		List<Transakcija> res = new ArrayList<>();
		try {
			List<Transakcija> unajmljivac = repo.findByUnajmljivac(user);
			res.addAll(unajmljivac);
		} catch (NullPointerException e) {
		}
		try {
			List<Transakcija> iznajmljivac = repo.findByIznajmljivac(user);
			res.addAll(iznajmljivac);
		} catch (NullPointerException e) {
		}
		
		return res;
	}

}
