package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.ZamjenaSlike;


@Repository
public interface ZamjenaSlikeRepository extends JpaRepository<ZamjenaSlike, Long> {
	
	@Query(value ="SELECT u FROM ZamjenaSlike u WHERE u.prijavljena = true")
	List<ZamjenaSlike> findAllByPrijavljena();
	
	List<ZamjenaSlike> findByRomobil_Owner(User user);
	
}
