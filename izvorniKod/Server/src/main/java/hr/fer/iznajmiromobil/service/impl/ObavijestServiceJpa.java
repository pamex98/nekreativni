package hr.fer.iznajmiromobil.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Obavijest;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;
import hr.fer.iznajmiromobil.repository.ObavijestRepository;
import hr.fer.iznajmiromobil.repository.UserRepository;
import hr.fer.iznajmiromobil.service.ObavijestService;

@Service
public class ObavijestServiceJpa implements ObavijestService {

	// error handling, optional ? , dodati exception handler

	@Autowired
	private ObavijestRepository repo;

	@Autowired
	private UserRepository userRepo;

	@Override
	public Obavijest createObavijest(Obavijest obavijest) {
		return repo.save(obavijest);
	}

	@Override
	public List<Obavijest> findByOwner(User owner) {
		List<Obavijest> res = repo.findByOwner(owner);
		Collections.reverse(res);
		return res;
	}

	@Override
	public List<Obavijest> createObavijestAdmin(String message) {
		List<User> admini = userRepo.findByRole(Role.ADMIN);
		List<Obavijest> noveObavijesti = new ArrayList<>();
		for (User f : admini) {
			Obavijest obavijest = new Obavijest();
			obavijest.setOwner(f);
			obavijest.setRead(false);
			obavijest.setTekst(message);
			noveObavijesti.add(obavijest);
			repo.save(obavijest);
		}
		return noveObavijesti;
	}

	@Override
	public Obavijest updateObavijest(Obavijest obavijest) {
		return repo.save(obavijest);
	}

}
