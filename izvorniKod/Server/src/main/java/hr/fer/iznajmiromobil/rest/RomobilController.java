package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.Romobil;
import hr.fer.iznajmiromobil.service.RomobilService;

@RestController
@RequestMapping("/api/romobili")
public class RomobilController {

	@Autowired
	private RomobilService service;

	@PostMapping("")
	public ResponseEntity<Romobil> createRomobil(@RequestBody Romobil romobil){
		Romobil saved = service.createRomobil(romobil);
		return ResponseEntity.created(URI.create("/romobili/"+saved.getId())).body(saved);
	}

	@PutMapping("/{id}")
	public Romobil updateRomobil(@RequestBody Romobil romobil, @PathVariable Long id) {
		if(!romobil.getId().equals(id)) {
			throw new IllegalArgumentException("Url nije jednak poslanom romobilu");
		}
		return service.updateRomobil(romobil);
	}
	
	@GetMapping("")
	public List<Romobil> getAll(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Romobil getOne(@PathVariable Long id) {
		return service.findById(id);
	}
	

}