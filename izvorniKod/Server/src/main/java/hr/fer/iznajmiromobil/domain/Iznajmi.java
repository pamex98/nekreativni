package hr.fer.iznajmiromobil.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Iznajmi {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@OneToOne
	private User klijent;

	@NotNull
	@OneToOne(cascade = CascadeType.MERGE)
	private Romobil romobil;

	private boolean klijentAccept;
	private boolean romobilOwnerAccept;

	public boolean isKlijentAccept() {
		return klijentAccept;
	}

	public void setKlijentAccept(boolean klijentAccept) {
		this.klijentAccept = klijentAccept;
	}

	public boolean isRomobilOwnerAccept() {
		return romobilOwnerAccept;
	}

	public void setRomobilOwnerAccept(boolean romobilOwnerAccept) {
		this.romobilOwnerAccept = romobilOwnerAccept;
	}

	public User getKlijent() {
		return klijent;
	}

	public void setKlijent(User klijent) {
		this.klijent = klijent;
	}

	public Romobil getRomobil() {
		return romobil;
	}

	public void setRomobil(Romobil romobil) {
		this.romobil = romobil;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((klijent == null) ? 0 : klijent.hashCode());
		result = prime * result + (klijentAccept ? 1231 : 1237);
		result = prime * result + ((romobil == null) ? 0 : romobil.hashCode());
		result = prime * result + (romobilOwnerAccept ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Iznajmi other = (Iznajmi) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (klijent == null) {
			if (other.klijent != null)
				return false;
		} else if (!klijent.equals(other.klijent))
			return false;
		if (klijentAccept != other.klijentAccept)
			return false;
		if (romobil == null) {
			if (other.romobil != null)
				return false;
		} else if (!romobil.equals(other.romobil))
			return false;
		if (romobilOwnerAccept != other.romobilOwnerAccept)
			return false;
		return true;
	}

}
