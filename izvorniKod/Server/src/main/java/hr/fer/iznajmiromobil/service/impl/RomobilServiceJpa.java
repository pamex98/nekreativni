package hr.fer.iznajmiromobil.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Romobil;
import hr.fer.iznajmiromobil.repository.RomobilRepository;
import hr.fer.iznajmiromobil.service.RomobilService;
import hr.fer.iznajmiromobil.service.exceptions.EntityAlreadyExistsException;

@Service
public class RomobilServiceJpa implements RomobilService {

	// error handling, optional ? , dodati exception handler

	@Autowired
	private RomobilRepository repo;

	@Override
	public Romobil createRomobil(Romobil romobil) {
		if (repo.existsByOwner(romobil.getOwner())) {
			throw new EntityAlreadyExistsException("Već imate romobil");
		}
		return repo.save(romobil);
	}

	@Override
	public Romobil updateRomobil(Romobil romobil) {
		return repo.save(romobil);
	}

	@Override
	public List<Romobil> findAll() {
		return repo.findAll();
	}

	@Override
	public Romobil findById(Long id) {
		return repo.findById(id).orElseThrow(() -> new IllegalArgumentException());
	}

}
