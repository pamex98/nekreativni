package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Poruka;

@Service
public interface PorukaService {
	
	Poruka createPoruka(Poruka poruka);
	
	List<Poruka> findByIznajmi(Iznajmi iznajmi);

}
