package hr.fer.iznajmiromobil.rest.DTO;

public class StringDTO {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "StringDTO [value=" + value + "]";
	}
	
	
	
}
