package hr.fer.iznajmiromobil.rest;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.service.IznajmiService;

@RestController
@RequestMapping("/api/iznajmi")
public class IznajmiController {

	@Autowired
	private IznajmiService service;

	@PostMapping("")
	public ResponseEntity<Iznajmi> createIznajmi(@RequestBody Iznajmi iznajmi) {
		Iznajmi saved = service.createIznajmi(iznajmi);
		return ResponseEntity.created(URI.create("/iznajmi/" + saved.getId())).body(saved);
	}
	
	//get nema body
	@PostMapping("/user")
	public Iznajmi getByUser(@RequestBody User user) {
		return service.findByUser(user);
	}
	
	
	//get nema body
	@PostMapping("/romobilowner")
	public Iznajmi getByRomobilOwner(@RequestBody User owner) {
		return service.findByRomobilOwner(owner);
	}

	@PutMapping("/{id}")
	public Iznajmi updateIznajmi(@RequestBody Iznajmi iznajmi, @PathVariable Long id) {
		if (!iznajmi.getId().equals(id)) {
			throw new IllegalArgumentException("Url nije jednak poslanom iznajmi-u");
		}
		return service.updateIznajmi(iznajmi);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteIznajmi(@PathVariable Long id){
		service.deleteIznajmi(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
