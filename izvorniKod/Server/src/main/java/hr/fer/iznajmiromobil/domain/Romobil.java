package hr.fer.iznajmiromobil.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Romobil {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(columnDefinition="LONGTEXT")
	private String urlPocetnaSlika;

	@Column(columnDefinition="LONGTEXT")
	private String urlTrenutnaSlika;
	
	@Column(nullable = false)
	private String currentLocation;
	
	@Column(nullable = false)
	private String returnLocation;
	
	@Column(nullable = false)
	private int penalty;
	
	@Column(nullable = false)
	private int priceKm;
	
	@Column(nullable = false)
	private Date dateReturn;
	
	@OneToOne
	private User owner;
	
	//ako je trenutno u tijeku iznajmi request onda nije visible
	private boolean visible;
	private boolean rent;
	private boolean izmijenjenaSlika;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public String getUrlPocetnaSlika() {
		return urlPocetnaSlika;
	}
	public void setUrlPocetnaSlika(String urlPocetnaSlika) {
		this.urlPocetnaSlika = urlPocetnaSlika;
	}
	public String getUrlTrenutnaSlika() {
		return urlTrenutnaSlika;
	}
	public void setUrlTrenutnaSlika(String urlTrenutnaSlika) {
		this.urlTrenutnaSlika = urlTrenutnaSlika;
	}
	public String getCurrentLocation() {
		return currentLocation;
	}
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}
	public String getReturnLocation() {
		return returnLocation;
	}
	public void setReturnLocation(String returnLocation) {
		this.returnLocation = returnLocation;
	}
	public int getPenalty() {
		return penalty;
	}
	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
	public int getPriceKm() {
		return priceKm;
	}
	public void setPriceKm(int priceKm) {
		this.priceKm = priceKm;
	}
	public Date getDateReturn() {
		return dateReturn;
	}
	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public boolean isRent() {
		return rent;
	}
	public void setRent(boolean rent) {
		this.rent = rent;
	}
	public boolean isIzmijenjenaSlika() {
		return izmijenjenaSlika;
	}
	public void setIzmijenjenaSlika(boolean izmijenjenaSlika) {
		this.izmijenjenaSlika = izmijenjenaSlika;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentLocation == null) ? 0 : currentLocation.hashCode());
		result = prime * result + ((dateReturn == null) ? 0 : dateReturn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (izmijenjenaSlika ? 1231 : 1237);
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + penalty;
		result = prime * result + priceKm;
		result = prime * result + (rent ? 1231 : 1237);
		result = prime * result + ((returnLocation == null) ? 0 : returnLocation.hashCode());
		result = prime * result + ((urlPocetnaSlika == null) ? 0 : urlPocetnaSlika.hashCode());
		result = prime * result + ((urlTrenutnaSlika == null) ? 0 : urlTrenutnaSlika.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Romobil other = (Romobil) obj;
		if (currentLocation == null) {
			if (other.currentLocation != null)
				return false;
		} else if (!currentLocation.equals(other.currentLocation))
			return false;
		if (dateReturn == null) {
			if (other.dateReturn != null)
				return false;
		} else if (!dateReturn.equals(other.dateReturn))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (izmijenjenaSlika != other.izmijenjenaSlika)
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (penalty != other.penalty)
			return false;
		if (priceKm != other.priceKm)
			return false;
		if (rent != other.rent)
			return false;
		if (returnLocation == null) {
			if (other.returnLocation != null)
				return false;
		} else if (!returnLocation.equals(other.returnLocation))
			return false;
		if (urlPocetnaSlika == null) {
			if (other.urlPocetnaSlika != null)
				return false;
		} else if (!urlPocetnaSlika.equals(other.urlPocetnaSlika))
			return false;
		if (urlTrenutnaSlika == null) {
			if (other.urlTrenutnaSlika != null)
				return false;
		} else if (!urlTrenutnaSlika.equals(other.urlTrenutnaSlika))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}
	
	
	
	
	
}
