package hr.fer.iznajmiromobil.rest;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;
import hr.fer.iznajmiromobil.rest.DTO.StringDTO;
import hr.fer.iznajmiromobil.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	
	@Autowired
	private UserService service;
	
	@PostMapping("")
	public ResponseEntity<User> createUser(@RequestBody User user){
		User saved = service.createUser(user);
		return ResponseEntity.created(URI.create("/users/"+saved.getId())).body(saved);
	}
	
	@PutMapping("")
	public List<User> deactivateAll(){
		service.deactivateAll();
		return service.findByRole(Role.USER);
	}
	
	@PutMapping("/{id}")
	public User updateUser(@PathVariable Long id, @RequestBody User user) {
		if(!user.getId().equals(id)) {
			throw new IllegalArgumentException("Url nije jednak poslanom useru");
		}
		return service.updateUser(user);
	}
	
	@GetMapping("")
	public List<User> getAll(){
		return service.findAll();
	}
	
	
	//get ne podržava body...
	@PostMapping("/role")
	public List<User> getByRole(@RequestBody StringDTO role){
		return service.findByRole(Role.valueOf(role.getValue()));
	}
	
	//get ne podržava body...
	@PostMapping("/active")
	public List<User> getByActive(@RequestBody StringDTO active){
		return service.findByActive(Boolean.valueOf(active.getValue()));
	}
	
	
	
}
