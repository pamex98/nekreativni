package hr.fer.iznajmiromobil.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.repository.UserRepository;
import hr.fer.iznajmiromobil.service.exceptions.EntityMissingException;


@Service
public class UserPrincipalDetailsService implements UserDetailsService {
	
	private UserRepository userRepo;
	
	public UserPrincipalDetailsService(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if(!userRepo.existsByUsername(username)) {
			throw new EntityMissingException("User ne postoji");
		}
		User user = userRepo.findByUsername(username);
		UserPrincipal userPrincipal = new UserPrincipal(user);
		return userPrincipal;
	}

}
