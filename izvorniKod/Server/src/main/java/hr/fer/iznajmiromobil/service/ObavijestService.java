package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Obavijest;
import hr.fer.iznajmiromobil.domain.User;

@Service
public interface ObavijestService {

	Obavijest createObavijest(Obavijest obavijest);

	List<Obavijest> findByOwner(User owner);

	List<Obavijest> createObavijestAdmin(String message);

	Obavijest updateObavijest(Obavijest obavijest);

}
