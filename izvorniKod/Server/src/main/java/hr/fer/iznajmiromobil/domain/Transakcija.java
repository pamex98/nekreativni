package hr.fer.iznajmiromobil.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Transakcija {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private User unajmljivac;
	
	@OneToOne
	private User iznajmljivac;
	
	//kaj s ovim, tu ili na frontendu izracunati?
	@Column(nullable = false)
	private long iznos;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUnajmljivac() {
		return unajmljivac;
	}

	public void setUnajmljivac(User unajmljivac) {
		this.unajmljivac = unajmljivac;
	}

	public User getIznajmljivac() {
		return iznajmljivac;
	}

	public void setIznajmljivac(User iznajmljivac) {
		this.iznajmljivac = iznajmljivac;
	}


	public long getIznos() {
		return iznos;
	}

	public void setIznos(long iznos) {
		this.iznos = iznos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((iznajmljivac == null) ? 0 : iznajmljivac.hashCode());
		result = prime * result + (int) (iznos ^ (iznos >>> 32));
		result = prime * result + ((unajmljivac == null) ? 0 : unajmljivac.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transakcija other = (Transakcija) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (iznajmljivac == null) {
			if (other.iznajmljivac != null)
				return false;
		} else if (!iznajmljivac.equals(other.iznajmljivac))
			return false;
		if (iznos != other.iznos)
			return false;
		if (unajmljivac == null) {
			if (other.unajmljivac != null)
				return false;
		} else if (!unajmljivac.equals(other.unajmljivac))
			return false;
		return true;
	}

	
	
	
}
