package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.OcjenaKomentar;
import hr.fer.iznajmiromobil.domain.User;

@Repository
public interface OcjenaKomentarRepository  extends JpaRepository<OcjenaKomentar, Long> {
	
	List<OcjenaKomentar> findByKlijent(User klijent);
	
	List<OcjenaKomentar> findByOcjenivac(User user);
}
