package hr.fer.iznajmiromobil.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class OcjenaKomentar {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	private User ocjenivac;
	
	private String komentar;
	

	private int ocjena;
	
	@ManyToOne
	private User klijent;
	
	private boolean ocijenjeno;

	
	
	
	public boolean isOcijenjeno() {
		return ocijenjeno;
	}

	public void setOcijenjeno(boolean ocijenjeno) {
		this.ocijenjeno = ocijenjeno;
	}

	public User getOcjenivac() {
		return ocjenivac;
	}

	public void setOcjenivac(User ocjenivac) {
		this.ocjenivac = ocjenivac;
	}

	

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public int getOcjena() {
		return ocjena;
	}

	public void setOcjena(int ocjena) {
		this.ocjena = ocjena;
	}

	public User getKlijent() {
		return klijent;
	}

	public void setKlijent(User klijent) {
		this.klijent = klijent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((klijent == null) ? 0 : klijent.hashCode());
		result = prime * result + ((komentar == null) ? 0 : komentar.hashCode());
		result = prime * result + (ocijenjeno ? 1231 : 1237);
		result = prime * result + ocjena;
		result = prime * result + ((ocjenivac == null) ? 0 : ocjenivac.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OcjenaKomentar other = (OcjenaKomentar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (klijent == null) {
			if (other.klijent != null)
				return false;
		} else if (!klijent.equals(other.klijent))
			return false;
		if (komentar == null) {
			if (other.komentar != null)
				return false;
		} else if (!komentar.equals(other.komentar))
			return false;
		if (ocijenjeno != other.ocijenjeno)
			return false;
		if (ocjena != other.ocjena)
			return false;
		if (ocjenivac == null) {
			if (other.ocjenivac != null)
				return false;
		} else if (!ocjenivac.equals(other.ocjenivac))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	
}
