package hr.fer.iznajmiromobil.service;


import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.User;

@Service
public interface IznajmiService {
	
	Iznajmi createIznajmi(Iznajmi zahtjev);
	
	Iznajmi findByUser(User user);
	
	Iznajmi findByRomobilOwner(User user);
	
	Iznajmi updateIznajmi(Iznajmi zahtjev);
	
	void deleteIznajmi(Long id);
	
}
