package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.OcjenaKomentar;
import hr.fer.iznajmiromobil.domain.User;

@Service
public interface OcjenaKomentarService {
	
	
	OcjenaKomentar createOcjenaKomentar(OcjenaKomentar ocjKomm);
	
	List<OcjenaKomentar> findByOwner(User user);
	
	List<OcjenaKomentar> findByOcjenivac(User user);
	
	OcjenaKomentar updateOcjenaKomentar(OcjenaKomentar ocjKomm);
	
}
