package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.User;
import hr.fer.iznajmiromobil.domain.enums.Role;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByUsername(String username);
	
	List<User> findByRole(Role role);
	
	List<User> findByIsActive(boolean active);
	
	boolean existsByUsername(String username);
	
}
