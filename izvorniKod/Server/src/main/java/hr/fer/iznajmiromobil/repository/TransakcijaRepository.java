package hr.fer.iznajmiromobil.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hr.fer.iznajmiromobil.domain.Transakcija;
import hr.fer.iznajmiromobil.domain.User;

@Repository
public interface TransakcijaRepository extends JpaRepository<Transakcija, Long> {
	
	List<Transakcija> findByUnajmljivac(User user);
	
	List<Transakcija> findByIznajmljivac(User user);
	
}	
