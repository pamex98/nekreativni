package hr.fer.iznajmiromobil.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Romobil;

@Service
public interface RomobilService {
	
	Romobil createRomobil(Romobil romobil);
	
	Romobil updateRomobil(Romobil romobil);
	
	List<Romobil> findAll();
	
	Romobil findById(Long id);
	
	
}
