package hr.fer.iznajmiromobil.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.iznajmiromobil.domain.Iznajmi;
import hr.fer.iznajmiromobil.domain.Poruka;
import hr.fer.iznajmiromobil.repository.PorukaRepository;
import hr.fer.iznajmiromobil.service.PorukaService;

@Service
public class PorukaServiceJpa implements PorukaService{
	
	
	@Autowired
	private PorukaRepository repo;
	
	@Override
	public Poruka createPoruka(Poruka poruka) {
		return repo.save(poruka);
	}

	@Override
	public List<Poruka> findByIznajmi(Iznajmi iznajmi) {
		return repo.findByUzZahtjev(iznajmi);
	}

}
