import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RomobilModel } from 'app/models/RomobilModel';
import { RomobilService } from 'app/services/romobil.service';
import { ErrorResponse } from 'app/services/error_response';
import { AuthService } from 'app/auth/auth.service';
import { IznajmiService } from 'app/services/iznajmi.service';
import { PorukaService } from 'app/services/poruka.service';
import { IznajmiModel } from 'app/models/IznajmiModel';
import { PorukaModel } from 'app/models/PorukaModel';
import { ObavijestService } from 'app/services/obavijest.service';
import { ObavijestModel } from 'app/models/ObavijestModel';
import { AlertService } from 'app/alert';

@Component({
  selector: 'app-ponuda',
  templateUrl: './ponuda.component.html',
  styleUrls: ['./ponuda.component.css']
})
export class PonudaComponent implements OnInit {

  show: boolean;
  romobil: RomobilModel;
  date: Date;

  constructor(
    private route: ActivatedRoute, private romobilService: RomobilService, private router: Router, public authService: AuthService
    , private iznajmiService: IznajmiService, private porukaService: PorukaService, private obavijestService: ObavijestService,
    private alertService: AlertService) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.romobilService.getOne(id).subscribe((romobil: RomobilModel) => {
      if (!romobil.visible) {
        this.show = false
        this.router.navigate(["/ponude"])
        this.alertService.warn("Romobil nije dostupan")
      } else {
        this.romobil = romobil;
        //this.date = new Date(this.romobil.dateReturn);
        this.show = true;
      }
    }, (error: ErrorResponse) => {
      this.show = false;
      this.router.navigate(["/ponude"])
      this.alertService.error("Romobil ne postoji")
    })
  }


  //provjera da je jedan click samo jer inace se crasha cijela logika jedan romobil-jedan owner-jedan klijent
  onSubmit(msg: string) {
    let klijentAccept = msg === ""
    console.log(klijentAccept)
    let iznajmi = new IznajmiModel(undefined, this.authService.getLoggedInUser(), this.romobil, klijentAccept, false);
    this.iznajmiService.createIznajmi(iznajmi).subscribe((res) => {
      this.romobil.visible = false;
      this.romobilService.updateRomobil(this.romobil).subscribe((r) => {
        console.log("romobil je updejtan")
      }, () => {
        console.log("romobil nije updejtan")
      })
      if (klijentAccept) {
        msg = "Želim iznajmiti Vaš romobil"
      }
      let poruka = new PorukaModel(undefined, this.authService.getLoggedInUser(), res, msg, undefined);
      this.porukaService.createPoruka(poruka).subscribe((p) => {
        let obavijest;
        if (klijentAccept) {
          obavijest = res.klijent.username + " želi iznajmiti Vaš romobil, pogledajte zahtjev"
        } else {
          obavijest = res.klijent.username + " poslao Vam je poruku"
        }
        this.obavijestService.createObavijest(new ObavijestModel(undefined, res.romobil.owner,
          obavijest, false)).subscribe(o => {

          })
        this.router.navigate(['/ponude/moja-ponuda'])
        this.alertService.success("Uspješno ste iznajmili romobil!")
      }, () => {
        console.log("poruka nije poslana")
      })
    }, (error: ErrorResponse) => {
      if (error.error.error === "Found") {
        console.log("vec imate zahtjev za iznajmljivanjem")
        this.router.navigate(['/ponude/moja-ponuda'])
        this.alertService.error("Možete imati samo jednu ponudu u tijeku, Vi je imate!")
      }
    })
  }

}

