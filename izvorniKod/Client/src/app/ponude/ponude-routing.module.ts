import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PonudeComponent } from './ponude.component';
import { MojePonudeComponent } from './moje-ponude/moje-ponude.component';
import { PretraziComponent } from './pretrazi/pretrazi.component';
import { PonudaComponent } from './ponuda/ponuda.component';
import { AuthGuard } from 'app/auth/auth.guard';


const routes: Routes = [
  {
    path: 'ponude', redirectTo: '/ponude/pretrazi', pathMatch: 'full'
  },
  {
    path: 'ponude', component: PonudeComponent, children: [
      {
        path: 'moja-ponuda', component: MojePonudeComponent, canActivate:[AuthGuard]
      },
      {
        path: 'pretrazi', component: PretraziComponent
      },
      {
        path: 'ponuda/:id', component: PonudaComponent
      }


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PonudeRoutingModule { }
