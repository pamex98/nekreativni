import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RomobilService } from 'app/services/romobil.service';
import { RomobilModel } from 'app/models/RomobilModel';
import { ErrorResponse } from 'app/services/error_response';

@Component({
  selector: 'app-pretrazi',
  templateUrl: './pretrazi.component.html',
  styleUrls: ['./pretrazi.component.css']
})
export class PretraziComponent implements OnInit {


  p: number = 1
  romobili: RomobilModel[] = [];
  listaprazna = true;
  //dodati kao neku poruku ak nema nijednog romobila(lista prazna, to dolje u subscribe nakon petlje odlucis)

  constructor(private romobilService: RomobilService) { }

  ngOnInit() {
    this.romobilService.getAll().subscribe((romobili: RomobilModel[]) => {
      for (let r of romobili) {
        if (!r.visible) {
          continue;
        }
        this.romobili.push(r)
        this.listaprazna = false;
      }
    }, (error: ErrorResponse) => {
      console.log(error.error.error)
    })
  }

}
