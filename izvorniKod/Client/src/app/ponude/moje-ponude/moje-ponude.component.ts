import { Component, OnInit } from '@angular/core';
import { IznajmiService } from 'app/services/iznajmi.service';
import { AuthService } from 'app/auth/auth.service';
import { IznajmiModel } from 'app/models/IznajmiModel';
import { PorukaModel } from 'app/models/PorukaModel';
import { OcjenaKomentarModel } from 'app/models/OcjenaKomentarModel';
import { ErrorResponse } from 'app/services/error_response';
import { PorukaService } from 'app/services/poruka.service';
import { OcjenaKomentarService } from 'app/services/ocjena-komentar.service';
import { ZamjenaSlikeModel } from 'app/models/ZamjenaSlikeModel';
import { ZamjenaSlikeService } from 'app/services/zamjena-slike.service';
import { Router } from '@angular/router';
import { RomobilService } from 'app/services/romobil.service';
import { ObavijestService } from 'app/services/obavijest.service';
import { ObavijestModel } from 'app/models/ObavijestModel';
import { AlertService } from 'app/alert';

@Component({
  selector: 'app-moje-ponude',
  templateUrl: './moje-ponude.component.html',
  styleUrls: ['./moje-ponude.component.css']
})
export class MojePonudeComponent implements OnInit {

  iznajmi: IznajmiModel;
  poruke: PorukaModel[] = [];
  ocjenaKomm: OcjenaKomentarModel[] = [];
  prosjecnaOcjena = 0
  showHasIznajmi = false;
  zamjenaSlike: ZamjenaSlikeModel = undefined
  show = false
  date: Date

  constructor(private iznajmiService: IznajmiService, private authService: AuthService,
    private porukaService: PorukaService, private ocjenaKommService: OcjenaKomentarService,
    private zamjenaService: ZamjenaSlikeService, private router: Router, private romobilService: RomobilService,
    private obavijestService: ObavijestService, private alertService: AlertService) { }



  ngOnInit() {
    setTimeout(() => {
      this.show = true
    }, 600)
    this.iznajmiService.getByUser(this.authService.getLoggedInUser()).subscribe((i) => {
      this.iznajmi = i;
      //this.date = new Date(this.iznajmi.romobil.dateReturn);
      this.porukaService.getByIznajmi(i).subscribe((p) => {
        this.poruke = p;
        this.ocjenaKommService.getByOwner(this.iznajmi.romobil.owner).subscribe((oK) => {
          for (let o of oK) {
            if (o.ocijenjeno) {
              this.ocjenaKomm.push(o)
            }
          }
          if (this.ocjenaKomm.length > 0) {
            this.prosjecnaOcjena = 0
            for (let o of this.ocjenaKomm) {
              this.prosjecnaOcjena = this.prosjecnaOcjena + o.ocjena
            }
            this.prosjecnaOcjena = this.prosjecnaOcjena / this.ocjenaKomm.length
          }
          this.zamjenaService.getZamjenaSlike(this.iznajmi.romobil.owner).subscribe(zamj => {
            for (let z of zamj) {
              if (z.romobil.id === this.iznajmi.romobil.id && z.adminDone === false) {
                this.zamjenaSlike = z
                break
              }
            }
            this.showHasIznajmi = true
          })
        }, (err) => {
          console.log("ocjene nisu loadane")
        })
      }, (err) => {
        console.log("poruke nisu loadane")
      })

    }, (error: ErrorResponse) => {
      if (error.error.error === 'Not Found') {
        this.showHasIznajmi = false;
      }
    })
  }

  urls = [];
  izmijenjenaSlika = false
  onFileSelected(event) {
    this.urls = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.urls.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
    this.izmijenjenaSlika = true
  }

  onZamjenaSlike(msg: string) {
    if (this.izmijenjenaSlika) {
      if (!this.iznajmi.romobil.izmijenjenaSlika && this.zamjenaSlike === undefined) {
        this.iznajmi.romobil.urlTrenutnaSlika = this.urls[0]
        this.iznajmi.romobil.izmijenjenaSlika = true;
        this.izmijenjenaSlika = false;
        let zamjenaSlike = new ZamjenaSlikeModel(undefined, this.iznajmi.romobil, msg,
          this.authService.getLoggedInUser(), false, this.urls[0], false)
        this.zamjenaService.createZamjenaSlike(zamjenaSlike).subscribe(r => {
          this.iznajmiService.updateIznajmi(this.iznajmi).subscribe(r => {
            this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.romobil.owner, this.iznajmi.klijent.username
              + " zamjenio je sliku Vašeg romobila!", false)).subscribe(o => {
                this.alertService.warn("Vaš prijedlog za izmjenu slike poslan je na odobravanje!")
              })

          })
        })
      } else {
        this.alertService.error("Slika romobila je već izmijenjena, čeka se odgovor vlasnika")
        this.izmijenjenaSlika = false
      }
    } else {
      this.alertService.error("Niste priložili zamjenu slike")
    }
  }

  onOdustani() {
    this.iznajmiService.deleteIznajmi(this.iznajmi).subscribe(() => {
      //popup kao vaš zahtjev je otkazan
      //moze nova obavijest vlasniku da je korisnik odustao od iznajmia
      this.iznajmi.romobil.visible = true;
      this.romobilService.updateRomobil(this.iznajmi.romobil).subscribe(r => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.romobil.owner,
          this.iznajmi.klijent.username + " odustao je od vašeg romobila", false)).subscribe(o => {
            this.router.navigate(['/ponude'])
            this.alertService.warn("Otkazali ste zahtjev")
          })
      })
    })
  }

  onPotvrdi() {
    this.iznajmi.klijentAccept = true;
    this.iznajmiService.updateIznajmi(this.iznajmi).subscribe(r => {
      let poruka = new PorukaModel(undefined, this.authService.getLoggedInUser(), r,
        "Želim iznajmiti Vaš romobil!", undefined);
      this.porukaService.createPoruka(poruka).subscribe(r => {
        this.poruke.push(r)
        this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.romobil.owner,
          this.iznajmi.klijent.username + " želi iznajmiti Vaš romobil, pogledajte zahtjev", false)).subscribe(o => {
            this.alertService.success("Uspješno ste poslali zahtjev, pričekajte vlasnika da odobri")
          })

      })
    })
  }

  sendMsg(msg: string) {
    let poruka = new PorukaModel(undefined, this.authService.getLoggedInUser(), this.iznajmi,
      msg, undefined);
    this.porukaService.createPoruka(poruka).subscribe(r => {
      this.poruke.push(r)
      this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.romobil.owner,
        this.iznajmi.klijent.username + " poslao Vam je poruku", false)).subscribe(o => {

        })
    })
  }



}
