import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ponude',
  templateUrl: './ponude.component.html',
  styleUrls: ['./ponude.component.css']
})
export class PonudeComponent implements OnInit {

  constructor() { }

  isActive = false;

  ngOnInit() {
    /*
    var listContainer = document.getElementById("lista-ponuda");

    // Get all buttons with class="btn" inside the container
    var lista = listContainer.getElementsByClassName("list-item");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < lista.length; i++) {
      lista[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        for (var j = 0; j <= current.length; j++) {
          current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
      });
    }
    */
  }

  toggleSidebar() {
    this.isActive = !this.isActive;
  }

}
