import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { PonudeRoutingModule } from './ponude-routing.module';
import { PonudaComponent } from './ponuda/ponuda.component';
import { MojePonudeComponent } from './moje-ponude/moje-ponude.component';
import { PretraziComponent } from './pretrazi/pretrazi.component';
import { PonudeComponent } from './ponude.component';
import { MatIconModule } from '@angular/material'
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [PonudaComponent, MojePonudeComponent,PretraziComponent, PonudeComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    MatIconModule,
    PonudeRoutingModule
  ]
})
export class PonudeModule { }
