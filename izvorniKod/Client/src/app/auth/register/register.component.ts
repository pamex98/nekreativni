import { Component, OnInit } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthService, User } from '../auth.service';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorResponse } from 'app/services/error_response';
import { AlertService } from 'app/alert';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'app/validator/MustMatch'
import { ObavijestService } from 'app/services/obavijest.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(private userService: UserService,
    private _location: Location,
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private obavijestService: ObavijestService) {
    if (this.authService.isUserLoggedIn()) {
      this._location.back()
      //neki popup "vec ste prijavljeni"
      this.alertService.warn("Vec ste prijavljeni!");
    }
  }

  urls = [];
  onFileSelected(event) {
    if (this.urls.length > 1) {
      this.urls = [];
    }
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.urls.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
    
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      ime: ['', Validators.required],
      prezime: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      broj_kartice: ['', Validators.required],
      lozinka: ['', [Validators.required, Validators.minLength(6)]],
      potvrdi_lozinku: ['', Validators.required],
      potvrda: ['', Validators.required]
    }, {
      validator: MustMatch('lozinka', 'potvrdi_lozinku')
    });
  }

  get f() { return this.registerForm.controls; }


  pictureError = false
  onRegister(form) {
    this.submitted = true;
    this.pictureError = false

    const username: string = form.value.username;
    const password: string = form.value.lozinka;
    const name: string = form.value.ime;
    const surname: string = form.value.prezime;
    const cardNumber: string = form.value.broj_kartice;
    const emailAdress: string = form.value.email;
    const urlOsobna: string = this.urls[0];
    const urlPotvrdaNekazna: string = this.urls[1];

    //provjeriti da su sva polja ispravna...
    if (this.registerForm.invalid) {
      return;
    }

    if (this.urls.length != 2) {
      this.pictureError = true
      return
    }

    //console.log(username, password, name, surname, cardNumber, emailAdress);
    console.log(username, password);
    let user = new UserModel(null, username, password, "USER", false, name, surname, cardNumber, emailAdress, urlOsobna, urlPotvrdaNekazna
      , true, true, true, true, true, true);

    this.userService.createUser(user).subscribe((res) => {
      this.obavijestService.createObavijestAdmin(username + " se registrirao i treba aktivaciju, pregledaj dokumente").subscribe(obav => {
      })
      this.router.navigate(['/ponude'])
      this.alertService.success("Pričekajte admina da vas aktivira kako bi se mogli loginati");
      //popup "pricekajte admina da vas aktivira kako bi se mogli loginati"
    }, (error: ErrorResponse) => {
      if (error.error.error === "Found") {
        console.log("Username vec postoji")
        //neki prozor za to prikazi
        this.alertService.warn("Username vec postoji");
      }
    })
  }

}
