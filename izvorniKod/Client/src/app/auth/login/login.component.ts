import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, User } from '../auth.service';
import { Location } from '@angular/common';
import { AlertService } from 'app/alert';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;

  username: string;
  password: string;
  errorMessage = 'Neispravni podaci';
  successMessage: string;
  loginSuccess = false;
  invalidLogin = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private _location: Location,
    private alertService: AlertService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    if(this.authenticationService.isUserLoggedIn())
      this._location.back()
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      lozinka: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  handleLogin(form) {
    this.submitted = true;

    const username: string = form.value.username;
    const password: string = form.value.lozinka;

    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.authenticationService(username, password).subscribe((result) => {
      this.loginSuccess = true;
      this.invalidLogin = false;
      this.successMessage = 'Prijavljen';
      this.router.navigate(['/ponude']);
      this.alertService.success(this.successMessage);
    }, () => {
      this.loginSuccess = false;
      this.invalidLogin = true;
    });
  }

}

