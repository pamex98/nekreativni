import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Location } from '@angular/common';
import { AlertService } from 'app/alert';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {


  constructor(private authService: AuthService, private router: Router, private _location:Location, private alertService: AlertService) { }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let user = this.authService.getLoggedInUser();
    if (user != null) {
      let role = user.role;
      if (role === "ADMIN")
        return true;
    }
    //this._location.back()
    this.router.navigate(['/ponude']);
    this.alertService.error("Niste ovlasteni!");
    return false;
  }

}
