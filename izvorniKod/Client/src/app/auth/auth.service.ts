import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { UserModel } from 'app/models/UserModel';
import { ObavijestService } from 'app/services/obavijest.service';



export interface User {
  id: number;
  username: string;
  password: string;
  isActive: boolean;
  role: string;
}
@Injectable({
  providedIn: 'root'
})


export class AuthService {

  AUTH_USER = 'authUser'
  HEADER = 'header'
  

  constructor(private http: HttpClient, private obavijestService:ObavijestService) {

  }

  authenticationService(username: String, password: String) {
    return this.http.get<UserModel>(`/api/security/auth`,
      { headers: { authorization: this.createBasicAuthToken(username, password) } }).pipe(map((res) => {
        this.registerSuccessfulLogin(res);
        this.obavijestService.getByOwner(this.getLoggedInUser()).subscribe(oba=>{
          let brojacNeprocitanih = 0
          for (let o of oba) {
            if (!o.read) {
              brojacNeprocitanih++
            }
          }
          this.obavijestService.setNeprocitane(brojacNeprocitanih)
        })
        
      }));
  }

  createBasicAuthToken(username: String, password: String) {
    let basicAuth = 'Basic ' + window.btoa(username + ":" + password);
    this.registerHeader(basicAuth);
    return this.getHeader();
  }

  registerHeader(header: string) {
    sessionStorage.setItem(this.HEADER, header);
  }

  getHeader(): string {
    return sessionStorage.getItem(this.HEADER);
  }

  registerSuccessfulLogin(user: UserModel) {
    sessionStorage.setItem(this.AUTH_USER, JSON.stringify(user));
  }

  logout() {
    sessionStorage.removeItem(this.AUTH_USER);
    sessionStorage.removeItem(this.HEADER);
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.AUTH_USER)
    if (user === null) return false
    return true
  }

  getLoggedInUser(): UserModel | null {
    let user = sessionStorage.getItem(this.AUTH_USER)
    if (user === null) return null
    return JSON.parse(user)
  }

  updateLoggedInUser(user:UserModel){
    sessionStorage.removeItem(this.AUTH_USER);
    sessionStorage.setItem(this.AUTH_USER, JSON.stringify(user));
  }

}

