import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertModule } from 'app/alert';

import { AdminModule } from './admin/admin.module';
import { IznajmiModule } from './iznajmi/iznajmi.module';
import { AuthModule } from './auth/auth.module';
import { PonudeModule } from './ponude/ponude.module';
import { ProfilModule } from './profil/profil.module';

import { StranicaNePostojiComponent } from './stranica-ne-postoji/stranica-ne-postoji.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth-intereceptor';
import { AuthService } from './auth/auth.service';
import { ErrorInterceptor } from './services/error_interceptor';
import { UserService } from './services/user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    StranicaNePostojiComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AdminModule,
    IznajmiModule,
    AuthModule,
    PonudeModule,
    ProfilModule,
    AlertModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor ,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
