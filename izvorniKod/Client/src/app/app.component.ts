import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { AlertService } from './alert';
import { ObavijestService } from './services/obavijest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthService]


})
export class AppComponent {



  constructor(public authService: AuthService, private router: Router, private alertService: AlertService,
    public obavijestService: ObavijestService) {

  }

  ngOnInit() {
    setInterval(() => {
      if (this.authService.isUserLoggedIn()) {
        this.obavijestService.getByOwner(this.authService.getLoggedInUser()).subscribe(res => {
          let brojacNeprocitanih = 0
          for (let o of res) {
            if (!o.read) {
              brojacNeprocitanih++
            }
          }
          this.obavijestService.setNeprocitane(brojacNeprocitanih)
        })
      }
    }, 5000)

  }

  logout() {
    this.authService.logout();
    this.obavijestService.deleteNeprocitane()
    this.router.navigate(['/login']);
    this.alertService.info("Odjavljen")
  }



}
