import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ObavijestModel } from 'app/models/ObavijestModel';
import { Observable } from 'rxjs';
import { UserModel } from 'app/models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class ObavijestService {

  private baseUrl: string = '/api/obavijesti'

  constructor(private http: HttpClient) { }

  createObavijest(obavijest: ObavijestModel): Observable<ObavijestModel> {
    return this.http.post<ObavijestModel>(this.baseUrl, obavijest)
  }

  getByOwner(user: UserModel): Observable<ObavijestModel[]> {
    return this.http.post<ObavijestModel[]>(this.baseUrl + '/owner', user)
  }

  updateObavijest(obavijest: ObavijestModel): Observable<ObavijestModel> {
    return this.http.put<ObavijestModel>(this.baseUrl, obavijest)
  }

  createObavijestAdmin(msg: string): Observable<ObavijestModel[]> {
    let data = {
      "value": msg
    }
    let body = JSON.stringify(data)
    return this.http.post<ObavijestModel[]>(this.baseUrl + '/admin', body, { headers: { 'Content-Type': 'application/json' } })
  }


  setNeprocitane(brojNeprocitanih: number) {
    sessionStorage.setItem('obavijesti', brojNeprocitanih.toString())
  }

  getNeprocitane(): number {
    let neprocitane = sessionStorage.getItem('obavijesti')
    if (neprocitane === null) {
      return -1;
    }
    return Number(neprocitane)
  }

  deleteNeprocitane(){
    sessionStorage.removeItem('obavijesti')
  }

}
