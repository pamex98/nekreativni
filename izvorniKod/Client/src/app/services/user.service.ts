import { Injectable } from '@angular/core';
import { UserModel } from 'app/models/UserModel';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private baseUrl: string = '/api/users'

  constructor(private http: HttpClient) { }

  createUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(this.baseUrl, user);
  }

  deactivateAll(): Observable<UserModel[]> {
    return this.http.put<UserModel[]>(this.baseUrl, "");
  }

  updateUser(user: UserModel): Observable<UserModel> {
    return this.http.put<UserModel>(this.baseUrl + '/' + user.id, user)
  }

  getAll(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.baseUrl);
  }

  getByRole(role: String): Observable<UserModel[]> {
    let data = {
      "value": role
    }
    let body = JSON.stringify(data)
    return this.http.post<UserModel[]>(this.baseUrl + '/role', body);
  }

  getByActive(active: boolean): Observable<UserModel[]> {
    let data = {
      "value": active
    }
    let body = JSON.stringify(data)
    return this.http.post<UserModel[]>(this.baseUrl + '/active', body);
  }


}
