import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IznajmiModel } from 'app/models/IznajmiModel';
import { Observable } from 'rxjs';
import { UserModel } from 'app/models/UserModel';
import { RomobilModel } from 'app/models/RomobilModel';

@Injectable({
  providedIn: 'root'
})
export class IznajmiService {

  private baseUrl: string = '/api/iznajmi'

  constructor(private http: HttpClient) { }

  createIznajmi(iznajmi: IznajmiModel): Observable<IznajmiModel> {
    return this.http.post<IznajmiModel>(this.baseUrl, iznajmi)
  }

  getByUser(user: UserModel): Observable<IznajmiModel> {
    return this.http.post<IznajmiModel>(this.baseUrl + '/user', user);
  }

  getByRomobilOwner(owner: UserModel): Observable<IznajmiModel> {
    return this.http.post<IznajmiModel>(this.baseUrl + '/romobilowner', owner);
  }

  updateIznajmi(iznajmi: IznajmiModel): Observable<IznajmiModel> {
    return this.http.put<IznajmiModel>(this.baseUrl + '/' + iznajmi.id, iznajmi);
  }

  deleteIznajmi(iznajmi: IznajmiModel) {
    return this.http.delete(this.baseUrl + '/' + iznajmi.id)
  }


}
