import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ZamjenaSlikeModel } from 'app/models/ZamjenaSlikeModel';
import { Observable } from 'rxjs';
import { UserModel } from 'app/models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class ZamjenaSlikeService {

  private baseUrl: string = '/api/zamjenaSlike'

  constructor(private http: HttpClient) { }

  createZamjenaSlike(zamjena: ZamjenaSlikeModel): Observable<ZamjenaSlikeModel> {
    return this.http.post<ZamjenaSlikeModel>(this.baseUrl, zamjena)
  }

  updateZamjenaSlike(zamjena: ZamjenaSlikeModel): Observable<ZamjenaSlikeModel> {
    return this.http.put<ZamjenaSlikeModel>(this.baseUrl + '/' + zamjena.id, zamjena)
  }

  getZamjenaSlike(user: UserModel): Observable<ZamjenaSlikeModel[]> {
    return this.http.post<ZamjenaSlikeModel[]>(this.baseUrl + '/user', user)
  }

  getByPrijavljene(): Observable<ZamjenaSlikeModel[]> {
    return this.http.get<ZamjenaSlikeModel[]>(this.baseUrl + '/prijavljene')
  }




}
