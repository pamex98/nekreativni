import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OcjenaKomentarModel } from 'app/models/OcjenaKomentarModel';
import { UserModel } from 'app/models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class OcjenaKomentarService {

  private baseUrl: string = '/api/ocjenaKomentar'

  constructor(private http: HttpClient) { }

  createOcjenaKomentar(ocjenaKomm: OcjenaKomentarModel): Observable<OcjenaKomentarModel> {
    return this.http.post<OcjenaKomentarModel>(this.baseUrl, ocjenaKomm)
  }

  getByOwner(user: UserModel): Observable<OcjenaKomentarModel[]> {
    return this.http.post<OcjenaKomentarModel[]>(this.baseUrl + '/owner', user)
  }

  getByOcjenivac(user: UserModel): Observable<OcjenaKomentarModel[]> {
    return this.http.post<OcjenaKomentarModel[]>(this.baseUrl + '/ocjenivac', user)
  }

  updateOcjenaKomm(ocjenaKomm: OcjenaKomentarModel): Observable<OcjenaKomentarModel> {
    return this.http.put<OcjenaKomentarModel>(this.baseUrl, ocjenaKomm)
  }


}
