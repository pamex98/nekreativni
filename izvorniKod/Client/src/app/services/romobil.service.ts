import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RomobilModel } from 'app/models/RomobilModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RomobilService {

  private baseUrl: string = '/api/romobili'

  constructor(private http: HttpClient) { }

  createRomobil(romobil: RomobilModel): Observable<RomobilModel> {
    return this.http.post<RomobilModel>(this.baseUrl, romobil)
  }

  updateRomobil(romobil: RomobilModel): Observable<RomobilModel> {
    return this.http.put<RomobilModel>(this.baseUrl + '/' + romobil.id, romobil)
  }

  getAll(): Observable<RomobilModel[]> {
    return this.http.get<RomobilModel[]>(this.baseUrl)
  }

  getOne(id: number): Observable<RomobilModel> {
    return this.http.get<RomobilModel>(this.baseUrl + '/' + id)
  }

}
