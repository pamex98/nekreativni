import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PorukaModel } from 'app/models/PorukaModel';
import { Observable } from 'rxjs';
import { IznajmiModel } from 'app/models/IznajmiModel';

@Injectable({
  providedIn: 'root'
})
export class PorukaService {

  private baseUrl: string = '/api/poruke'

  constructor(private http: HttpClient) { }

  createPoruka(poruka: PorukaModel): Observable<PorukaModel> {
    return this.http.post<PorukaModel>(this.baseUrl, poruka)
  }

  getByIznajmi(iznajmi: IznajmiModel): Observable<PorukaModel[]> {
    return this.http.post<PorukaModel[]>(this.baseUrl + '/iznajmi', iznajmi)
  }


}
