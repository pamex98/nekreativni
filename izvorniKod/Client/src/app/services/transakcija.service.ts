import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TransakcijaModel } from 'app/models/TransakcijaModel';
import { Observable } from 'rxjs';
import { UserModel } from 'app/models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class TransakcijaService {

  private baseUrl: string = '/api/transakcije'

  constructor(private http: HttpClient) { }

  createTransakcija(transakcija: TransakcijaModel): Observable<TransakcijaModel> {
    return this.http.post<TransakcijaModel>(this.baseUrl, transakcija)
  }

  getByUserTransakcija(user: UserModel): Observable<TransakcijaModel[]> {
    return this.http.post<TransakcijaModel[]>(this.baseUrl + '/user', user)
  }

}
