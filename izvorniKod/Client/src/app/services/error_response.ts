import { HttpErrorResponse } from '@angular/common/http';

export interface ErrorResponse extends HttpErrorResponse{
    error: {
        id?: string
        links?: { about: string }
        status: string
        message?: string
        path?: string
        error?: string
        code?: string
        title: string
        detail: string
        source?: {
           pointer?: string
           parameter?: string
        }
        meta?: any
        }
}