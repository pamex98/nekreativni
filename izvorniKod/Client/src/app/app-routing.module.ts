import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StranicaNePostojiComponent } from './stranica-ne-postoji/stranica-ne-postoji.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: '**', component: StranicaNePostojiComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
