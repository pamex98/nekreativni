import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmiAdminComponent } from './iznajmi-admin.component';

describe('IznajmiAdminComponent', () => {
  let component: IznajmiAdminComponent;
  let fixture: ComponentFixture<IznajmiAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmiAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmiAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
