import { Component, OnInit } from '@angular/core';
import { ZamjenaSlikeService } from 'app/services/zamjena-slike.service';
import { RomobilService } from 'app/services/romobil.service';
import { ObavijestService } from 'app/services/obavijest.service';
import { ZamjenaSlikeModel } from 'app/models/ZamjenaSlikeModel';
import { AlertService } from 'app/alert';
import { ObavijestModel } from 'app/models/ObavijestModel';

@Component({
  selector: 'app-iznajmi-admin',
  templateUrl: './iznajmi-admin.component.html',
  styleUrls: ['./iznajmi-admin.component.css']
})
export class IznajmiAdminComponent implements OnInit {

  zamjene: ZamjenaSlikeModel[] = []

  constructor(private zamjenaService: ZamjenaSlikeService, private romobilService: RomobilService
    , private obavijestService: ObavijestService, private alertService: AlertService) { }

  ngOnInit() {
    this.zamjenaService.getByPrijavljene().subscribe(zam => {
      for (let z of zam) {
        if (!z.adminDone) {
          this.zamjene.push(z)
        }
      }
    })
  }

  onPrihvati(zamjena: ZamjenaSlikeModel) {
    for (let i = 0; i < this.zamjene.length; i++) {
      if (this.zamjene[i].id === zamjena.id) {
        this.zamjene.splice(i, 1);
        break;
      }
    }
    zamjena.adminDone = true
    zamjena.romobil.urlTrenutnaSlika = zamjena.romobil.urlPocetnaSlika
    this.romobilService.updateRomobil(zamjena.romobil).subscribe(rom => {
      this.zamjenaService.updateZamjenaSlike(zamjena).subscribe(zamj => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, zamjena.inicijator, "Admin je odbio Vašu izmjenu slike, vraćena je prethodna slika", false)).
          subscribe(o => {
            this.obavijestService.createObavijest(new ObavijestModel(undefined, zamjena.romobil.owner,
              "Admin je prihvatio Vašu prijavu izmjene slike, vraćena je prethodna slika", false)).subscribe(t => {
                this.alertService.success("Prihvatili ste prijavu slike, slika romobila je vraćena")
              })
          })
      })
    })
  }

  onOdbij(zamjena: ZamjenaSlikeModel) {
    for (let i = 0; i < this.zamjene.length; i++) {
      if (this.zamjene[i].id === zamjena.id) {
        this.zamjene.splice(i, 1);
        break;
      }
    }
    zamjena.adminDone = true
    zamjena.romobil.urlPocetnaSlika = zamjena.romobil.urlTrenutnaSlika
    this.romobilService.updateRomobil(zamjena.romobil).subscribe(rom => {
      this.zamjenaService.updateZamjenaSlike(zamjena).subscribe(zamj => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, zamjena.inicijator, "Admin je odobrio Vašu zamjenu slike", false)).
          subscribe(o => {
            this.obavijestService.createObavijest(new ObavijestModel(undefined, zamjena.romobil.owner,
              "Admin je odbio prijavu izmjene slike, izmijenjena slika ostaje", false)).subscribe(t => {
                this.alertService.success("Odbili ste prijavu slike, slika romobila ostaje ista")
              })
          })
      })
    })
  }

}
