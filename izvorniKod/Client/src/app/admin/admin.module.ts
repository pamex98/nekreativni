import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { IznajmiAdminComponent } from './iznajmi-admin/iznajmi-admin.component';
import { ProfilAdminComponent } from './profil-admin/profil-admin.component';
import { AdminComponent } from './admin.component';


@NgModule({
  declarations: [IznajmiAdminComponent, ProfilAdminComponent, AdminComponent],
  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
