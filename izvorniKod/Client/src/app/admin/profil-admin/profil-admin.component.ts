import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/auth/auth.service';
import { UserService } from 'app/services/user.service';
import { UserModel } from 'app/models/UserModel';
import { stringify } from 'querystring';
import { AlertService } from 'app/alert';

@Component({
  selector: 'app-profil-admin',
  templateUrl: './profil-admin.component.html',
  styleUrls: ['./profil-admin.component.css']
})
export class ProfilAdminComponent implements OnInit {

  users: UserModel[] = []
  date: number = Date.now()
  constructor(private authService: AuthService, private userService: UserService, private alertService: AlertService) { }

  ngOnInit() {
    this.userService.getAll().subscribe(u => {
      for (let user of u) {
        if (user.role != 'ADMIN') {
          this.users.push(user)
        }
      }
    })
  }

  onSpremi() {
    for (let user of this.users) {
      this.userService.updateUser(user).subscribe(() => {
      })

    }
    this.alertService.success("Promjene spremljene");
  }

  change(event) {
    let elementId: string = (event.target as Element).id;
    /*  const element = document.getElementById(elementId) as HTMLInputElement
     */
    const id = Number(elementId)
    for (let user of this.users) {
      if (user.id === id) {
        user.active = !user.active;
        break
      }
    }
  }

  deactivateAll() {
    for (let user of this.users) {
      user.active = false;
    }
  }
}