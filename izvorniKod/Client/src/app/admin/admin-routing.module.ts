import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IznajmiAdminComponent } from './iznajmi-admin/iznajmi-admin.component';
import { ProfilAdminComponent } from './profil-admin/profil-admin.component';
import { AdminGuard } from 'app/auth/admin.guard';


const routes: Routes = [
  {path:'iznajmi-admin', canActivate:[AdminGuard], component: IznajmiAdminComponent},
  {path:'profil-admin', canActivate:[AdminGuard], component: ProfilAdminComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
