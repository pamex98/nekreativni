import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OcjeniKlijentaComponent } from './ocjeni-klijenta.component';

describe('OcjeniKlijentaComponent', () => {
  let component: OcjeniKlijentaComponent;
  let fixture: ComponentFixture<OcjeniKlijentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OcjeniKlijentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OcjeniKlijentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
