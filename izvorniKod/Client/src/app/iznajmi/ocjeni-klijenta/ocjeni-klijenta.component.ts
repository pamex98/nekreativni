import { Component, OnInit } from '@angular/core';
import { OcjenaKomentarService } from 'app/services/ocjena-komentar.service';
import { OcjenaKomentarModel } from 'app/models/OcjenaKomentarModel';
import { AuthService } from 'app/auth/auth.service';
import { AlertService } from 'app/alert';
import { ObavijestService } from 'app/services/obavijest.service';
import { ObavijestModel } from 'app/models/ObavijestModel';

@Component({
  selector: 'app-ocjeni-klijenta',
  templateUrl: './ocjeni-klijenta.component.html',
  styleUrls: ['./ocjeni-klijenta.component.css']
})
export class OcjeniKlijentaComponent implements OnInit {

  ocjenaKomentari: OcjenaKomentarModel[] = []
  neocijenjeni = 0


  constructor(private ocjenaKommService: OcjenaKomentarService,
    private authService: AuthService, private alertService: AlertService, private obavijestService: ObavijestService) { }

  ngOnInit() {
    this.ocjenaKommService.getByOcjenivac(this.authService.getLoggedInUser()).subscribe(res => {
      for (let r of res) {
        if (!r.ocijenjeno) {
          this.ocjenaKomentari.push(r)
          this.neocijenjeni++
        }
      }
    })
  }

  onSubmit(msg: string, selected: string, ocjenaKomentar: OcjenaKomentarModel) {
    if (msg === "" || selected === "") {
      this.alertService.error("Morate dati ocjenu i komentar!")
      return
    }
  
    ocjenaKomentar.ocijenjeno = true
    ocjenaKomentar.komentar = msg
    let ocjena = Number(selected)
    ocjenaKomentar.ocjena = ocjena
    this.ocjenaKommService.updateOcjenaKomm(ocjenaKomentar).subscribe(r => {
      this.obavijestService.createObavijest(new ObavijestModel(undefined, ocjenaKomentar.klijent,
        ocjenaKomentar.ocjenivac.username + " Vas je ocijenio i ostavio komentar na Vašem profilu", false)).subscribe(o => {
          for(let i = 0; i < this.ocjenaKomentari.length; i++) {
            if (this.ocjenaKomentari[i].id === ocjenaKomentar.id) {
                this.ocjenaKomentari.splice(i, 1);
                break;
            }
          }
          this.neocijenjeni = this.neocijenjeni - 1
          this.alertService.success("Ocijenili ste korisnika " + ocjenaKomentar.klijent.username)
  
          
        })
    })

  }

}
