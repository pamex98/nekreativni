import { Component, OnInit } from '@angular/core';
import { RomobilModel } from 'app/models/RomobilModel';
import { AuthService } from 'app/auth/auth.service';
import { ErrorResponse } from 'app/services/error_response';
import { RomobilService } from 'app/services/romobil.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { IznajmiService } from 'app/services/iznajmi.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ZamjenaSlikeService } from 'app/services/zamjena-slike.service';
import { ZamjenaSlikeModel } from 'app/models/ZamjenaSlikeModel';
import { ObavijestService } from 'app/services/obavijest.service';
import { AlertService } from 'app/alert';


@Component({
  selector: 'app-iznajmi-romobil',
  templateUrl: './iznajmi-romobil.component.html',
  styleUrls: ['./iznajmi-romobil.component.css']
})
export class IznajmiRomobilComponent implements OnInit {

  registerForm: FormGroup;
  azurirajForm: FormGroup;
  romobil: RomobilModel;
  hasRomobil: boolean = false;
  show = false;
  submitted = false;
  zamjenaSlike: ZamjenaSlikeModel = undefined

  constructor(private romobilService: RomobilService, private authService: AuthService,
    private router: Router, private iznajmiService: IznajmiService,
    private formBuilder: FormBuilder, private zamjenaService: ZamjenaSlikeService
    , private obavijestService: ObavijestService, private alertService: AlertService) { }

  ngOnInit() {
    setTimeout(() => {
      this.show = true
    }, 600)
    this.romobilService.getAll().subscribe((res) => {
      const currentUser = this.authService.getLoggedInUser();
      for (let r of res) {
        let romobilOwner = r.owner
        //usernameovi su unique pa na temelju njih comparamo jednakost objekata..
        if (currentUser.username === romobilOwner.username) {
          this.romobil = r;

          if (!this.romobil.visible) {

            //romobil nije vidljiv u poundama
            this.iznajmiService.getByRomobilOwner(this.romobil.owner).subscribe(res => {

              //romobil je u procesuiranju, redirect na zahtjev za iznamljivanje
              //popup "upravljajte zahtjevom za vaš trenutni romobil"
              this.router.navigate(['/iznajmi/zahtjevi-iznajmljivanje'])
              this.alertService.info("Imate zahtjev za Vaš romobil!")
            }, (err: ErrorResponse) => {

              if (err.error.error === 'Not Found') {

                this.hasRomobil = true;

                //nema iznajmia(prilikom finisha iznajmia -> iznajmi se brise i sve pripadne poruke)
                //omoguci useru da ponovno stavi romobil isti i moze mijenjati neke fieldove..
              }

            })
          } else {
            //romobil vidljiv u ponudama, omoguci owneru da moze updejtati neke fieldove 
            this.hasRomobil = true;

          }
          break;
        }
      }
      this.zamjenaService.getZamjenaSlike(this.authService.getLoggedInUser()).subscribe(zamjene => {
        for (let zamjena of zamjene) {
          if (!zamjena.adminDone) {
            this.zamjenaSlike = zamjena
            break;
          }

        }
      })
    });
    
    this.registerForm = this.formBuilder.group({
      currentLocation: ['', Validators.required],
      returnLocation: ['', Validators.required],
      dateReturn: ['', Validators.required],
      priceKM: ['', Validators.required],
      penalty: ['', Validators.required]
    });
    this.azurirajForm = this.formBuilder.group({
      currentLocation: ['', Validators.required],
      returnLocation: ['', Validators.required],
      dateReturn: ['', Validators.required],
      priceKM: ['', Validators.required],
      penalty: ['', Validators.required]
    });
  }


  urls = [];
  izmijenjenaSlika = false
  onFileSelected(event) {
    this.urls = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.urls.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
    this.izmijenjenaSlika = true
    this.pictureError = false
  }

  get f() { return this.registerForm.controls; }
  get a() { return this.azurirajForm.controls; }



  pictureError = false
  onUnesiRomobil(form) {
    this.submitted = true;
    this.pictureError = false

    const urlPocetna = this.urls[0]
    const urlTrenutna = this.urls[0]
    const currentLocation = form.value.currentLocation
    const returnLocation = form.value.returnLocation
    const penalty = form.value.penalty
    const priceKM = form.value.priceKM
    var date = new Date(form.value.dateReturn)
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
    if (this.urls.length === 0) {
      this.pictureError = true
      return;
    }
    if (this.registerForm.invalid) {
      return;
    }

    //GUMB DA ne moze vise stisnuti (npr rapid fire...)

    let noviRomobil = new RomobilModel(null, urlPocetna, urlTrenutna, currentLocation, returnLocation, penalty, priceKM, date, this.authService.getLoggedInUser(), true, false, false)

    this.romobilService.createRomobil(noviRomobil).subscribe(res => {
      console.log("romobil sejvan")
      this.router.navigate(['/ponude/ponuda/' + res.id])
      this.alertService.success("Uspješno ste unijeli romobil")
    }, err => {
      if (err.error.error) {
        console.log("vec imate romobil")
      }
      console.log("romobil nije sejvan")
    })
  }


  //popravi datum field (ne radi jer dateformat-local)
  onUpdateRomobil(form) {
    this.submitted = true;
    //provjera jedan klik, sve ispravno...
    if (this.azurirajForm.invalid) {
      return;
    }


    if (this.izmijenjenaSlika) {
      this.romobil.urlPocetnaSlika = this.urls[0]
      this.romobil.urlTrenutnaSlika = this.urls[0]
    }
    this.romobil.visible = true;
    this.romobil.rent = false
    this.romobil.izmijenjenaSlika = false
    if (this.zamjenaSlike != undefined) {
      this.zamjenaSlike.adminDone = true
      this.zamjenaSlike.romobil = this.romobil
      this.zamjenaService.updateZamjenaSlike(this.zamjenaSlike).subscribe(r => {
        if (this.zamjenaSlike.prijavljena) {
          this.obavijestService.createObavijestAdmin(this.zamjenaSlike.romobil.owner.username + " ažurirao je romobil, čime je prijava zamjene slike od " + this.zamjenaSlike.inicijator.username + " poništena.").subscribe(o => {
          })
        }
      })
    }
    this.romobilService.updateRomobil(this.romobil).subscribe(r => {
      console.log("owner updejtao romobil")
      this.router.navigate(['/ponude/ponuda/' + this.romobil.id])
      this.alertService.success("Uspješno ste ažurirali romobil")
    })
  }

}
