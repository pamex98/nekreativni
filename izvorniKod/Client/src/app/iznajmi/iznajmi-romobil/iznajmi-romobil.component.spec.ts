import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmiRomobilComponent } from './iznajmi-romobil.component';

describe('IznajmiRomobilComponent', () => {
  let component: IznajmiRomobilComponent;
  let fixture: ComponentFixture<IznajmiRomobilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmiRomobilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmiRomobilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
