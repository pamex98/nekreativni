import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtjevIznajmljivanjeComponent } from './zahtjev-iznajmljivanje.component';

describe('ZahtjevIznajmljivanjeComponent', () => {
  let component: ZahtjevIznajmljivanjeComponent;
  let fixture: ComponentFixture<ZahtjevIznajmljivanjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZahtjevIznajmljivanjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtjevIznajmljivanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
