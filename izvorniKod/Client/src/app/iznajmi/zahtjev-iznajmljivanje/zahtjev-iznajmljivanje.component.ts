import { Component, OnInit } from '@angular/core';
import { ErrorResponse } from 'app/services/error_response';
import { IznajmiService } from 'app/services/iznajmi.service';
import { AuthService } from 'app/auth/auth.service';
import { OcjenaKomentarService } from 'app/services/ocjena-komentar.service';
import { PorukaService } from 'app/services/poruka.service';
import { ZamjenaSlikeService } from 'app/services/zamjena-slike.service';
import { Router } from '@angular/router';
import { RomobilService } from 'app/services/romobil.service';
import { ObavijestService } from 'app/services/obavijest.service';
import { IznajmiModel } from 'app/models/IznajmiModel';
import { PorukaModel } from 'app/models/PorukaModel';
import { OcjenaKomentarModel } from 'app/models/OcjenaKomentarModel';
import { ZamjenaSlikeModel } from 'app/models/ZamjenaSlikeModel';
import { ObavijestModel } from 'app/models/ObavijestModel';
import { RomobilModel } from 'app/models/RomobilModel';
import { TransakcijaService } from 'app/services/transakcija.service';
import { TransakcijaModel } from 'app/models/TransakcijaModel';
import { AlertService } from 'app/alert';


@Component({
  selector: 'app-zahtjev-iznajmljivanje',
  templateUrl: './zahtjev-iznajmljivanje.component.html',
  styleUrls: ['./zahtjev-iznajmljivanje.component.css']
})
export class ZahtjevIznajmljivanjeComponent implements OnInit {

  iznajmi: IznajmiModel;
  poruke: PorukaModel[] = [];
  ocjenaKomm: OcjenaKomentarModel[] = [];
  prosjecnaOcjena = 0
  showHasIznajmi = false;
  showRomobil = false
  zamjenaSlike: ZamjenaSlikeModel = undefined
  show = false
  date: Date
  

  romobil: RomobilModel

  constructor(private iznajmiService: IznajmiService, private authService: AuthService,
    private porukaService: PorukaService, private ocjenaKommService: OcjenaKomentarService,
    private zamjenaService: ZamjenaSlikeService, private router: Router, private romobilService: RomobilService,
    private obavijestService: ObavijestService, private transakcijaService: TransakcijaService,
    private alertService: AlertService) { }



  ngOnInit() {
    setTimeout(()=>{
      this.show = true
    }, 600)
    this.romobilService.getAll().subscribe(r => {
      this.zamjenaService.getZamjenaSlike(this.authService.getLoggedInUser()).subscribe(zamjene => {
        for (let zamjena of zamjene) {
          if (!zamjena.prijavljena) {
            if (!zamjena.adminDone) {
              this.zamjenaSlike = zamjena
              break;
            }
          }
        }
      })
      for (let rom of r) {
        if (rom.owner.username === this.authService.getLoggedInUser().username) {
          this.romobil = rom
          //this.date = new Date(this.romobil.dateReturn);
          this.showRomobil = true
          break
        }
      }
      this.iznajmiService.getByRomobilOwner(this.authService.getLoggedInUser()).subscribe((i) => {
        this.iznajmi = i;
        this.porukaService.getByIznajmi(i).subscribe((p) => {
          this.poruke = p;
          this.ocjenaKommService.getByOwner(this.iznajmi.klijent).subscribe((oK) => {
            for (let o of oK) {
              if (o.ocijenjeno) {
                this.ocjenaKomm.push(o)
              }
            }
            if (this.ocjenaKomm.length > 0) {
              this.prosjecnaOcjena = 0
              for (let o of this.ocjenaKomm) {
                this.prosjecnaOcjena = this.prosjecnaOcjena + o.ocjena
              }
              this.prosjecnaOcjena = this.prosjecnaOcjena / this.ocjenaKomm.length
            }
            this.showHasIznajmi = true
          }, (err) => {
            console.log("ocjene nisu loadane")
          })
        }, (err) => {
          console.log("poruke nisu loadane")
        })

      }, (error: ErrorResponse) => {
        if (error.error.error === 'Not Found') {
          this.showHasIznajmi = false;
        }
      })
    })

  }




  onOdustani() {
    this.iznajmiService.deleteIznajmi(this.iznajmi).subscribe(() => {
      this.iznajmi.romobil.visible = true;
      this.romobilService.updateRomobil(this.iznajmi.romobil).subscribe(r => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.klijent,
          this.romobil.owner.username + " odbio je Vaš zahtjev za iznajmljivanjem", false)).subscribe(o => {
            this.router.navigate(['/iznajmi'])
            this.alertService.warn("Otkazali ste zahtjev")
          })
      })
    })
  }

  onPotvrdi() {
    this.romobil.izmijenjenaSlika = false
    this.romobil.urlPocetnaSlika = this.romobil.urlTrenutnaSlika
    this.romobil.rent = true
    this.iznajmi.romobilOwnerAccept = true
    if (this.zamjenaSlike != undefined) {
      this.zamjenaSlike.adminDone = true
      this.zamjenaSlike.romobil = this.romobil
      this.zamjenaService.updateZamjenaSlike(this.zamjenaSlike).subscribe(r => {
        if (this.zamjenaSlike.prijavljena) {
          this.obavijestService.createObavijestAdmin(this.zamjenaSlike.romobil.owner.username + " prihvatio je zahtjev, a time i promjenu slike koju je prijavio korisniku " + this.zamjenaSlike.inicijator.username).subscribe(r => { })
        }
      })
    }
    this.iznajmiService.deleteIznajmi(this.iznajmi).subscribe(i => {
      this.romobilService.updateRomobil(this.romobil).subscribe(rom => {
        this.transakcijaService.createTransakcija(new TransakcijaModel(undefined, this.iznajmi.klijent, this.romobil.owner,
          this.romobil.priceKm * 10)).subscribe(trans => {
            this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.klijent,
              this.romobil.owner.username + " iznajmio Vam je romobil, transakcija spremljena", false)).subscribe(oba => {
                this.ocjenaKommService.createOcjenaKomentar(new OcjenaKomentarModel(undefined, this.romobil.owner, "", undefined, this.iznajmi.klijent, false)).subscribe(ocjene => {
                  this.router.navigate(['/iznajmi/ocjeni-klijenta'])
                  this.alertService.success("Prihvatili ste zahtjev, ocijenite klijenta!");
                })

              })
          })
      })
    })
  }

  sendMsg(msg: string) {
    let poruka = new PorukaModel(undefined, this.romobil.owner, this.iznajmi,
      msg, undefined);
    this.porukaService.createPoruka(poruka).subscribe(r => {
      this.poruke.push(r)
      this.obavijestService.createObavijest(new ObavijestModel(undefined, this.iznajmi.klijent,
        this.romobil.owner.username + " poslao Vam je poruku u vezi Vaše ponude", false)).subscribe(o => {

        })
    })
  }

  onPrihvatiIzmjenu() {
    this.romobil.izmijenjenaSlika = false
    this.zamjenaSlike.adminDone = true
    this.romobil.urlPocetnaSlika = this.romobil.urlTrenutnaSlika
    this.zamjenaSlike.romobil = this.romobil
    this.romobilService.updateRomobil(this.romobil).subscribe(rom => {
      this.zamjenaService.updateZamjenaSlike(this.zamjenaSlike).subscribe(zamj => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, this.zamjenaSlike.inicijator, this.romobil.owner.username
          + " je prihvatio Vašu izmjenu njegovog romobila", false)).subscribe(izmj => {
            this.alertService.success("Prihvatili ste izmjenu slike")

          })
      })
    })
  }

  onOdbijIzmjenu() {
    this.romobil.izmijenjenaSlika = false
    this.zamjenaSlike.prijavljena = true
    this.zamjenaSlike.romobil = this.romobil
    this.romobilService.updateRomobil(this.romobil).subscribe(rom => {
      this.zamjenaService.updateZamjenaSlike(this.zamjenaSlike).subscribe(zamj => {
        this.obavijestService.createObavijest(new ObavijestModel(undefined, this.zamjenaSlike.inicijator, this.romobil.owner.username
          + " je prijavio Vašu izmjenu slike adminu!", false)).subscribe(izmj => {
            this.obavijestService.createObavijestAdmin(this.romobil.owner.username + " prijavio je izmjenu slike korisnika " +
              this.zamjenaSlike.inicijator.username).subscribe(r => {
                this.alertService.warn("Prijavili ste izmjenu slike!")
              })

          })
      })
    })
  }

}
