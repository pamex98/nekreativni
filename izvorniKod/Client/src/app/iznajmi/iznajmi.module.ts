import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material'

import { IznajmiRoutingModule } from './iznajmi-routing.module';
import { IznajmiRomobilComponent } from './iznajmi-romobil/iznajmi-romobil.component';
import { ZahtjevIznajmljivanjeComponent } from './zahtjev-iznajmljivanje/zahtjev-iznajmljivanje.component';

import { OcjeniKlijentaComponent } from './ocjeni-klijenta/ocjeni-klijenta.component';

import { IznajmiComponent } from './iznajmi.component';

@NgModule({
  declarations: [IznajmiRomobilComponent, ZahtjevIznajmljivanjeComponent, OcjeniKlijentaComponent,  IznajmiComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    IznajmiRoutingModule
  ]
})
export class IznajmiModule { }
