import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IznajmiComponent } from './iznajmi.component';
import { IznajmiRomobilComponent } from './iznajmi-romobil/iznajmi-romobil.component';

import { OcjeniKlijentaComponent } from './ocjeni-klijenta/ocjeni-klijenta.component';

import { ZahtjevIznajmljivanjeComponent } from './zahtjev-iznajmljivanje/zahtjev-iznajmljivanje.component';
import { AuthGuard } from 'app/auth/auth.guard';



const routes: Routes = [
  { path: 'iznajmi', redirectTo: '/iznajmi/iznajmi-romobil', pathMatch: 'full' },
  {

    path: 'iznajmi', component: IznajmiComponent, canActivate:[AuthGuard], canActivateChild:[AuthGuard], children: [
      { path: 'iznajmi-romobil', component: IznajmiRomobilComponent },
     
      { path: 'ocjeni-klijenta', component: OcjeniKlijentaComponent },
      {
        path: 'zahtjevi-iznajmljivanje', component: ZahtjevIznajmljivanjeComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IznajmiRoutingModule { }
