import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmiComponent } from './iznajmi.component';

describe('IznajmiComponent', () => {
  let component: IznajmiComponent;
  let fixture: ComponentFixture<IznajmiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
