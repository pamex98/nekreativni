import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilComponent } from './profil.component';
import { MojprofilComponent } from './mojprofil/mojprofil.component';
import { ObavijestiComponent } from './obavijesti/obavijesti.component';
import { PostavkePrivatnostiComponent } from './postavke-privatnosti/postavke-privatnosti.component';
import { TransakcijeComponent } from './transakcije/transakcije.component';
import { AuthGuard } from 'app/auth/auth.guard';


const routes: Routes = [

  { path: 'profil', redirectTo: '/profil/mojprofil', pathMatch: 'full' },
  {
    path: 'profil', component: ProfilComponent, canActivate:[AuthGuard], canActivateChild:[AuthGuard], children: [
      { path: 'mojprofil', component: MojprofilComponent },
      { path: 'obavijesti', component: ObavijestiComponent },
      { path: 'postavke-privatnosti', component: PostavkePrivatnostiComponent },
      { path: 'transakcije', component: TransakcijeComponent }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilRoutingModule { }
