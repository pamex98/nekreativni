import { Component, OnInit } from '@angular/core';
import { TransakcijaService } from 'app/services/transakcija.service';
import { AuthService } from 'app/auth/auth.service';
import { TransakcijaModel } from 'app/models/TransakcijaModel';

@Component({
  selector: 'app-transakcije',
  templateUrl: './transakcije.component.html',
  styleUrls: ['./transakcije.component.css']
})
export class TransakcijeComponent implements OnInit {

  transakcije: TransakcijaModel[] = []

  constructor(private transakcijaService: TransakcijaService, public authService: AuthService) { }

  ngOnInit() {
    this.transakcijaService.getByUserTransakcija(this.authService.getLoggedInUser()).subscribe(r => {
      this.transakcije = r;
    }, err => {

    })
  }

}
