import { Component, OnInit } from '@angular/core';
import { ObavijestService } from 'app/services/obavijest.service';
import { ObavijestModel } from 'app/models/ObavijestModel';
import { AuthService } from 'app/auth/auth.service';
import { AlertService } from 'app/alert';

@Component({
  selector: 'app-obavijesti',
  templateUrl: './obavijesti.component.html',
  styleUrls: ['./obavijesti.component.css']
})
export class ObavijestiComponent implements OnInit {

  obavijesti: ObavijestModel[] = []
  unreadMsg: number = 0
  show = false

  constructor(private obavijestService: ObavijestService, private authService: AuthService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.obavijestService.getByOwner(this.authService.getLoggedInUser()).subscribe(r => {
      this.obavijesti = r
      if (this.obavijesti.length > 0) {
        this.checkUnreadMsgs()
        this.show = true
      } else {
        this.show = false
      }
    }, err => {

    })
  }

  checkUnreadMsgs() {
    this.unreadMsg = 0
    for (let obavijest of this.obavijesti) {
      if (!obavijest.read) {
        this.unreadMsg += 1;
      }
    }
  }

  markAllAsRead() {
    for (let obavijest of this.obavijesti) {
      obavijest.read = true;
    }
    this.checkUnreadMsgs()

  }

  change(event) {
    let elementId: string = (event.target as Element).id;
    /*  const element = document.getElementById(elementId) as HTMLInputElement
     */
    const id = Number(elementId)
    for (let o of this.obavijesti) {
      if (o.id === id) {
        o.read = !o.read
        break
      }
    }
    this.checkUnreadMsgs()
  }

  onSpremi() {
    for (let o of this.obavijesti) {
      this.obavijestService.updateObavijest(o).subscribe(r => {
        this.obavijestService.setNeprocitane(this.unreadMsg)
      }, err => {

      })
    }
    this.alertService.success("Promjene spremljene")
    //neki popup "uspjesno spremljeno nakon kaj ih sve updejta"
  }


}
