import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { ProfilRoutingModule } from './profil-routing.module';
import { ObavijestiComponent } from './obavijesti/obavijesti.component';
import { MojprofilComponent } from './mojprofil/mojprofil.component';
import { PostavkePrivatnostiComponent } from './postavke-privatnosti/postavke-privatnosti.component';
import { TransakcijeComponent } from './transakcije/transakcije.component';
import { ProfilComponent } from './profil.component';


@NgModule({
  declarations: [ObavijestiComponent, MojprofilComponent, PostavkePrivatnostiComponent, TransakcijeComponent, ProfilComponent],
  imports: [
    CommonModule,
    FormsModule,
    ProfilRoutingModule
  ]
})
export class ProfilModule { }
