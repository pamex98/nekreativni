import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  isActive = false;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    /*
    // Get the container element
    var listContainer = document.getElementById("lista-iznajmi");

    // Get all buttons with class="btn" inside the container
    var lista = listContainer.getElementsByClassName("list-item");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < lista.length; i++) {
      lista[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active");
        for (var j = 0; j <= current.length; j++) {
          current[0].className = current[0].className.replace(" active", "");
        }
        this.className += " active";
      });
    }
    */
  }

  toggleSidebar() {
    this.isActive = !this.isActive;
  }

}
