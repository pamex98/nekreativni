import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/auth/auth.service';
import { UserService } from 'app/services/user.service';
import { UserModel } from 'app/models/UserModel';
import { AlertService } from 'app/alert';

@Component({
  selector: 'app-postavke-privatnosti',
  templateUrl: './postavke-privatnosti.component.html',
  styleUrls: ['./postavke-privatnosti.component.css']
})
export class PostavkePrivatnostiComponent implements OnInit {

  user: UserModel;

  constructor(private authService: AuthService, private userService: UserService, private alertService: AlertService) { }

  ngOnInit() {
    this.user = this.authService.getLoggedInUser()
  }

  change(event) {
    let elementId: string = (event.target as Element).id;
    switch (elementId) {
      case "name": {
        this.user.namePrivate = !this.user.namePrivate
        break;
      }
      case "surname": {
        this.user.surnamePrivate = !this.user.surnamePrivate
        break;
      }
      case "cardNumber": {
        this.user.cardNumberPrivate = !this.user.cardNumberPrivate
        break;
      }
      case "emailAdress": {
        this.user.emailAdressPrivate = !this.user.emailAdressPrivate
        break;
      }
      case "osobna": {
        this.user.osobnaPrivate = !this.user.osobnaPrivate
        break;
      }
      case "potvrdaNekazna": {
        this.user.potvrdaNekaznaPrivate = !this.user.potvrdaNekaznaPrivate
        break;
      }
    }
  }

  onSpremi() {
    this.userService.updateUser(this.user).subscribe(r => {
      this.authService.updateLoggedInUser(r)
      this.alertService.success("Promjene spremljene")
    }, err => {

    })
  }


}
