import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostavkePrivatnostiComponent } from './postavke-privatnosti.component';

describe('PostavkePrivatnostiComponent', () => {
  let component: PostavkePrivatnostiComponent;
  let fixture: ComponentFixture<PostavkePrivatnostiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostavkePrivatnostiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostavkePrivatnostiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
