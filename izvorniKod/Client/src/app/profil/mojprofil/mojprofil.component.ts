import { Component, OnInit } from '@angular/core';
import { AuthService, User } from 'app/auth/auth.service';
import { UserModel } from 'app/models/UserModel';
import { OcjenaKomentarService } from 'app/services/ocjena-komentar.service';
import { OcjenaKomentarModel } from 'app/models/OcjenaKomentarModel';
import { ErrorResponse } from 'app/services/error_response';


@Component({
  selector: 'app-mojprofil',
  templateUrl: './mojprofil.component.html',
  styleUrls: ['./mojprofil.component.css']
})
export class MojprofilComponent implements OnInit {

  public user: UserModel;
  ocjenaKomm: OcjenaKomentarModel[] = []
  prosjecnaOcjena: number = 0

  constructor(private authService: AuthService, private ocjenaKommService: OcjenaKomentarService) { }

  ngOnInit() {
    this.user = this.authService.getLoggedInUser()
    this.ocjenaKommService.getByOwner(this.user).subscribe(r => {
      for(let o of r){
        if(o.ocijenjeno){
          this.ocjenaKomm.push(o)
        }
      }
      if (this.ocjenaKomm.length != 0) {
        for (let ocjenaKomm of this.ocjenaKomm) {
          this.prosjecnaOcjena = this.prosjecnaOcjena + ocjenaKomm.ocjena
        }
        this.prosjecnaOcjena = this.prosjecnaOcjena / this.ocjenaKomm.length
      }

    }, (err:ErrorResponse) => {
    })
  }



}
