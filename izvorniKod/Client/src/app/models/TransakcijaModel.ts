import { UserModel } from './UserModel';
import { RomobilModel } from './RomobilModel';

export class TransakcijaModel {
    constructor(public id: number, public unajmljivac: UserModel, public iznajmljivac: UserModel, public iznos: number) {
    }
}