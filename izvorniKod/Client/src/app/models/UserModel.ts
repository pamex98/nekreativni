export class UserModel {
    constructor(public id: number,
        public username: string, public password: string,
        public role: string, public active: boolean,

        public name: string, public surname: string,
        public cardNumber: string, public emailAdress: string,
        public urlOsobna: string, public urlPotvrdaNekazna: string,
        public namePrivate: boolean, public surnamePrivate: boolean,
        public cardNumberPrivate: boolean, public emailAdressPrivate: boolean,
        public osobnaPrivate: boolean, public potvrdaNekaznaPrivate: boolean, ) {
    }

}