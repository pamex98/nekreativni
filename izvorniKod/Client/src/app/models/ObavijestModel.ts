import { UserModel } from './UserModel';

export class ObavijestModel {
    constructor(public id: number, public owner: UserModel, public tekst: string, public read: boolean) {
    }
}