import { RomobilModel } from './RomobilModel';
import { UserModel } from './UserModel';

export class ZamjenaSlikeModel {
    constructor(public id: number, public romobil: RomobilModel, public komentar: string, public inicijator: UserModel, public prijavljena: boolean, public urlZamjenjenaSlika: string, public adminDone: boolean) {
    }
}