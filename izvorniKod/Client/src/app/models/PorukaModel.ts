import { UserModel } from './UserModel';
import { IznajmiModel } from './IznajmiModel';

export class PorukaModel {
    constructor(public id:number,public posiljatelj: UserModel, public uzZahtjev: IznajmiModel,public poruka: string,public datumPoruke: Date) {
    }
}