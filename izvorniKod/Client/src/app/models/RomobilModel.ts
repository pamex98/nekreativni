import { UserModel } from './UserModel';

export class RomobilModel {
    constructor(public id: number, public urlPocetnaSlika: string, public urlTrenutnaSlika: string,
        public currentLocation: string, public returnLocation: string,
        public penalty: number, public priceKm: number, public dateReturn: Date,
        public owner: UserModel, public visible: boolean, public rent: boolean, public izmijenjenaSlika: boolean) {
    }
}