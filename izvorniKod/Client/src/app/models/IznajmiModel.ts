import { UserModel } from './UserModel';
import { RomobilModel } from './RomobilModel';

export class IznajmiModel {
    constructor(public id: number, public klijent: UserModel, public romobil: RomobilModel, public klijentAccept: boolean, public romobilOwnerAccept: boolean) {
    }
}