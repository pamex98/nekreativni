import { UserModel } from './UserModel';

export class OcjenaKomentarModel {
    constructor(public id: number, public ocjenivac: UserModel, public komentar: string, public ocjena: number, public klijent: UserModel, public ocijenjeno: boolean) {
    }
}