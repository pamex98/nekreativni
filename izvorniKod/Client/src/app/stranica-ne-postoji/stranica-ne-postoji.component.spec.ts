import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StranicaNePostojiComponent } from './stranica-ne-postoji.component';

describe('StranicaNePostojiComponent', () => {
  let component: StranicaNePostojiComponent;
  let fixture: ComponentFixture<StranicaNePostojiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StranicaNePostojiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StranicaNePostojiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
